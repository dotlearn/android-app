/**
 * Automatically generated file. DO NOT MODIFY
 */
package org.dotlearn.app;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "org.dotlearn.app";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 9;
  public static final String VERSION_NAME = "";
}
