package org.dotlearn.app;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.regex.Pattern;
import java.util.zip.ZipFile;

import org.dotlearn.app.models.CourseFile;
import org.dotlearn.app.models.JSONTree;
import org.dotlearn.app.util.ImageDB;
import org.dotlearn.app.util.JSONdb;
import org.dotlearn.app.util.LessonDB;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Application;
import android.content.SharedPreferences;

public class Global extends Application {
	
	private String zipPath;
	private ZipFile zip;
	private JSONObject course;
	private JSONObject lesson;
	private SharedPreferences userData;
	private JSONObject lessonProgress;
	private JSONObject courseStorage;
	private CourseFile courseFile;

	private ImageDB imDB;
	private JSONTree tree;
	private LessonDB lessonDB;


	public  void setLessonDB(LessonDB ldb){

		lessonDB = ldb;

	}

	public  LessonDB getLessonDB(){

		return  lessonDB;
	}

	public  void setImDB(ImageDB imdb){
		imDB  = imdb;
	}

	public  ImageDB getImDB(){
		return  imDB;
	}


	public  void setTree(JSONTree tree){

		this.tree = tree;
	}

	public  JSONTree getTree(){

		return  tree;
	}
	
	public ZipFile getZip(){
		
		return zip;
	}
	
	public JSONObject getCourse(){
		
		return course;
	}
	
	public String getZipPath(){
		
		return zipPath;
	}
	
	public void setZip(String path){
		
		zipPath = path;
		
		try {
			zip = new ZipFile(zipPath);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
	
	
	public void deleteFiles(){
	      

	    JSONdb db = new JSONdb(this);

		File folder = new File(db.storageFolder);
		DeleteRecursive(folder);
	}
	
	void DeleteRecursive(File fileOrDirectory) {
	    if (fileOrDirectory.isDirectory())
	        for (File child : fileOrDirectory.listFiles())
	            DeleteRecursive(child);

	    fileOrDirectory.delete();
	}
	
	public void setCourseFile(String path) throws IOException{
		
		this.courseFile = new CourseFile(path, getBaseContext());
	}
	
	public CourseFile getCourseFile(){
		
		return this.courseFile;
	}

	
	public void setLesson(JSONObject lessonObject){
		
		lesson = lessonObject;
		
		String id = lesson.optString("id", lesson.optString("name", "???"));   
		
		
		   if (courseStorage == null){
			   getUserData();
		   }
		  lessonProgress = courseStorage.optJSONObject(id);  // Retrieve course with the specified ID
		   
		   if (lessonProgress== null){           // If no such course exists, create a new one
			   lessonProgress = generateNewLessonProgress();
		   }

		
	}
	
	public JSONObject getLesson(){
		
	    return lesson;	
	}
	
	

	
	public JSONObject generateNewLessonProgress(){
		   
		String id = lesson.optString("id", lesson.optString("name", "???")); 
		
		
		lessonProgress = new JSONObject();
		
		   try {
			   
			lessonProgress.put("id", id);
			lessonProgress.put("name", lesson.optString("name"));
			

				JSONArray items = lesson.getJSONArray("items");
				JSONArray itemsProgress = new JSONArray();
				
				for (int j=0; j<items.length(); j++){
					
					JSONObject itemProgress = new JSONObject();
					JSONObject item = items.getJSONObject(j);
					
					itemProgress.put("label", item.optString("label"));
					itemProgress.put("module", item.optString("module"));
					itemProgress.put("complete", false);
					itemProgress.put("userData", new JSONObject());
					itemsProgress.put(itemProgress);

				}
				lessonProgress.put("items", itemsProgress);
	
			
			
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
	
			
			return lessonProgress;
		
	
		   
		
	}
	
	public void deleteUserData(){
		
		SharedPreferences.Editor editor = userData.edit();
		editor.clear();
		editor.commit();
	}
	
	
	public void getUserData(){
		userData = getSharedPreferences("userData", 0);
		
		String jsonString = userData.getString("courseStorage", "");
	
		try {
			
			if (jsonString == ""){
				courseStorage = new JSONObject();
			} else{
				courseStorage = new JSONObject(jsonString);
			}
			
	
	
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void saveUserData(){
		
		SharedPreferences.Editor editor = userData.edit();
		
		
		try {
			courseStorage.put(lessonProgress.getString("id"), lessonProgress);
			
			String jsonString = courseStorage.toString();
			
			editor.putString("courseStorage", jsonString);
			editor.commit();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void setItemData(int itemIndex, JSONObject data){
		

		
		JSONArray items;
		try {
			items = lessonProgress.getJSONArray("items");
			JSONObject item = items.getJSONObject(itemIndex);
			item.put("userData", data);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		this.saveUserData();
		
		
		
	}
	
	public JSONObject getItemData(int itemIndex){
		JSONObject data = new JSONObject();
		
		JSONArray items;
		try {
			items = lessonProgress.getJSONArray("items");
			JSONObject item = items.getJSONObject(itemIndex);
			data = item.getJSONObject("userData");
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		return data;
	}

	public void importFile(String fileName) throws IOException, JSONException {
		// TODO Auto-generated method stub
		

		CourseFile file = new CourseFile(fileName, getBaseContext());
		
		
		JSONObject tempjson = file.json;
		
		String id = tempjson.getString("id");
		
		String image = tempjson.getString("image");
		
		String[] URIparts = image.split(Pattern.quote("."));
		String extension = URIparts[URIparts.length -1];
		
	   
		JSONdb db = new JSONdb(this);
		
		String destLoc = db.storageFolder.concat("images/").concat(id).concat(".").concat(extension);
		
		File dest = new File (destLoc);
		

		if(!dest.exists()){
			dest.createNewFile();
		}
		
	
		
		
		InputStream in = file.getMetaData(image);
		OutputStream out = new FileOutputStream(dest);
		
    	// Transfer bytes from in to out
    	byte[] buf = new byte[1024];
    	int len;
    	while ((len = in.read(buf)) > 0) {
    	    out.write(buf, 0, len);
    	}
    	in.close();
    	out.close();
    	
    	
   
    	LessonDB manager = new LessonDB(this);
    	manager.putLesson(tempjson);
    	
    	//then update the file Upload, okay?
	}
	
	
	/*
	public void setItemComplete(int lessonIndex, int itemIndex, boolean complete){
		
		try {
			JSONArray lessons = courseProgress.getJSONArray("lessons");
			JSONObject  lesson = lessons.getJSONObject(lessonIndex);
			JSONArray items = lesson.getJSONArray("items");
			JSONObject item = items.getJSONObject(itemIndex);
			
			item.put("complete", complete);
		    items.put(itemIndex, item);
		    lesson.put("items", items);
		    lessons.put(lessonIndex, lesson);
		    courseProgress.put("lessons", lessons);
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		saveUserData();
	}

	
	
	public int[] getQuizResponse(int lessonIndex, int itemIndex){
		int[] answers = new int[0];
		
		try {
			JSONObject lesson = courseProgress.getJSONArray("lessons").getJSONObject(lessonIndex);
			JSONObject item = lesson.getJSONArray("items").getJSONObject(itemIndex);
			JSONArray response = item.optJSONArray("response");
			if (response != null ){
						answers = new int[response.length()];
						
						for (int i=0; i<response.length(); i++){
						 answers[i] =response.getInt(i);
						}
			}				
			
		} catch (JSONException e) {

			e.printStackTrace();
		}
		
		return answers;
		
	}
		*/
	
	
	/*
	public JSONObject generateNewCourseProgress(){
		   
		String id = course.optString("id", course.optString("name", "???")); 
		
		
		courseProgress = new JSONObject();
		
		   try {
			   
			courseProgress.put("id", id);
			courseProgress.put("name", course.optString("name"));
			
			JSONArray lessons = course.getJSONArray("lessons");
			JSONArray lessonsProgress = new JSONArray();
			
			for (int i=0; i<lessons.length(); i++){
				
				JSONObject lessonProgress = new JSONObject();
				JSONObject lesson = lessons.getJSONObject(i);
				lessonProgress.put("name", lesson.optString("name"));
				lessonProgress.put("complete", false);
				
				JSONArray items = lesson.getJSONArray("items");
				JSONArray itemsProgress = new JSONArray();
				
				for (int j=0; j<items.length(); j++){
					
					JSONObject itemProgress = new JSONObject();
					JSONObject item = items.getJSONObject(j);
					
					itemProgress.put("label", item.optString("label"));
					itemProgress.put("type", item.optString("type"));
					itemProgress.put("complete", false);
		
					if (item.opt("type")=="module/quiz"){
						itemProgress.put("response", new JSONArray());
					}
					itemsProgress.put(itemProgress);

				}
				lessonProgress.put("items", itemsProgress);
				lessonsProgress.put(lessonProgress);
				
			}
			
			courseProgress.put("lessons", lessonsProgress);
			
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
	
			
			return courseProgress;
		
	
		   
		
	}*/
	
	
	
	
	
	
	/*
	public void setCourse(JSONObject courseObject){
		
		course = courseObject;
		
		   // Remember the course by it's ID, or if e-course doesn't supply ID, by name, and if it fails that, than by ???
			String id = course.optString("id", course.optString("name", "???"));   
		 
		   if (courseStorage == null){
			   getUserData();
		   }
		   courseProgress = courseStorage.optJSONObject(id);  // Retrieve course with the specified ID
		   
		   if (courseProgress== null){           // If no such course exists, create a new one
			   courseProgress = generateNewCourseProgress();
		   }

            
		
	}
	*/
	
}
