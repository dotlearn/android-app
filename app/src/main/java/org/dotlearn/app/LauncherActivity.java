package org.dotlearn.app;


import android.support.v7.app.ActionBarActivity;
import android.content.Intent;

import android.os.Bundle;
import android.os.Handler;


import android.view.View;



public class LauncherActivity extends ActionBarActivity {
	
	public String selectedLanguage;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
//		getSupportActionBar().hide();

		setContentView(R.layout.activity_waiting);
		
		
		final Intent intent = new Intent(this, org.dotlearn.app.MyLibraryActivity.class);
		
		final Handler handler = new Handler();
		
		handler.postDelayed(new Runnable() {
		  @Override
		  public void run() {		
			startActivity(intent);
		  }
		}, 0);
		
		((Global)this.getApplication()).getUserData();  // Startup Activity
	
		
	}
	
	
	public void go2Home(View view){
		Intent intent = new Intent(this, org.dotlearn.app.HomeActivity.class);
		startActivity(intent);
	}
	

	
	


}
	
