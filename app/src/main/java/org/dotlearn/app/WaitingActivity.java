package org.dotlearn.app;


import org.dotlearn.app.models.JSONTree;
import org.dotlearn.app.util.AsyncResponse;
import org.dotlearn.app.util.FetchJSON;
import org.dotlearn.app.util.ImageDB;
import org.dotlearn.app.util.LessonDB;
import org.dotlearn.app.util.TreeDB;
import org.json.JSONArray;
import org.json.JSONException;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import  android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import java.io.IOException;


public class WaitingActivity extends Activity  implements AsyncResponse{
	FetchJSON fetchJSON;
	
	Activity activity;

	ImageDB imdb;
	LessonDB ldb;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_waiting);



		System.out.println("Checking connectivity");







		if (!isNetworkAvailable()){




			System.out.println("Network not available");

			onError("Network Error", "There was an error connecting to the internet. You need to be connected to the internet to access the library");
//			goHome();

		} else{

			System.out.println("Network available");
			System.out.println("Fetching lessons from server");

			activity = this;
			fetchJSON = new FetchJSON(this);


			if(getString(R.string.server_lessons_endpoint).compareTo("")==0){
				onError("Config Error", "Webserver not set in res/values/config.xml");
			} else {

				String address = getString(R.string.server_lessons_endpoint);

				String addresses[] = {address};

				fetchJSON.execute(addresses);

				onWaitingSetup();


			}







		}



			



	}

	private  void onWaitingSetup(){     // While waiting for fetch JSON


		try {
			imdb = new ImageDB(this);
			ldb = new LessonDB(this);


		} catch (IOException|JSONException e) {

			onError("Database Error", "An error ocurrred while loading the database: " + e.getMessage());
			e.printStackTrace();
		}



	}


	


	public  void onError(String title, String message){

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);


		final Context mcontext  = this;

		// set title
		alertDialogBuilder.setTitle(title);

		// set dialog message
		alertDialogBuilder.setMessage(message);

		alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				//do things

				Intent intent = new Intent(mcontext, org.dotlearn.app.MyLibraryActivity.class);
				intent.putExtra("error", true);
				startActivity(intent);

			}
		});


		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();

	}

	public  void goHome(){
		Intent intent = new Intent(this, org.dotlearn.app.MyLibraryActivity.class);
		intent.putExtra("error", false);
		startActivity(intent);
	}

	@Override
	public void processFinish(String output) {


		System.out.println("Got the server response");

		if(output.length() > 0){


			System.out.println("Server response is good:");

			try {
				JSONArray treeArray = new JSONArray(output);


				TreeDB db = new TreeDB(this);
				db.putTree(treeArray);

				JSONTree tree = new JSONTree(treeArray);

				final Global global =((Global) this.getApplication());

				global.setTree(tree);
				global.setImDB(imdb);
				global.setLessonDB(ldb);


				Intent intent = new Intent(this, org.dotlearn.app.WebStoreActivity.class);
				intent.putExtra("selected", "0");        //Initialize at the root
				startActivity(intent);


			} catch (JSONException e) {
				e.printStackTrace();
				onError("Server Error", "We apologize - there was an error with the server. Please try again later");
			}




		} else{

			onError("Network Error", "There was an error connecting to the internet. You need to be connected to the internet to access the library");

		}

	}


	//Check if the Internet is connected
	private boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager
				= (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
}
