package org.dotlearn.app;

import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.os.Bundle;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.support.v4.widget.DrawerLayout;
import org.dotlearn.app.fragments.*;


public class CourseBrowserActivity extends ActionBarActivity implements
		LessonDrawerFragment.NavigationDrawerCallbacks {

	/**
	 * Fragment managing the behaviors, interactions and presentation of the
	 * navigation drawer.
	 */
	private LessonDrawerFragment lessonDrawer;

	/**
	 * Used to store the last screen title. For use in
	 * {@link #restoreActionBar()}.
	 */
	private CharSequence mTitle;
	
	public ZipFile zip;
	public JSONObject course;
	public JSONArray lessons;
	public int selectedLesson;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_course_browser);

		lessonDrawer = (LessonDrawerFragment) getSupportFragmentManager()
				.findFragmentById(R.id.navigation_drawer);
		
		mTitle = getTitle();
	    
		// Set up the drawer.
		lessonDrawer.setUp(R.id.navigation_drawer,
				(DrawerLayout) findViewById(R.id.drawer_layout));

 	     Intent intent = getIntent();   // Get stuff from the intent  
	     selectedLesson = intent.getIntExtra("selected", 0);
	    
	    try {
			zip = ((Global)this.getApplication()).getZip();
            course = ((Global)this.getApplication()).getCourse();
            lessons = course.getJSONArray("lessons");
			
            int length = lessons.length();
            String [] lessonTitles = new String[length];
            
            for (int i =0; i<length; i++){
            	JSONObject thislesson = lessons.getJSONObject(i);
            	String thislessonTitle = thislesson.getString("name");
            	lessonTitles[i] = thislessonTitle;
            }
            
            lessonDrawer.setLessons(lessonTitles);
            previewLesson(selectedLesson);
            
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    

	}

	@Override
	public void onNavigationDrawerItemSelected(int position) {
		
		if (lessons != null){
		   previewLesson(position);
		} else{
			System.out.println("lessons  null");	
		}

	}



	public void restoreActionBar() {
		ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle(mTitle);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (!lessonDrawer.isDrawerOpen()) {
			// Only show items in the action bar relevant to this screen
			// if the drawer is not showing. Otherwise, let the drawer
			// decide what to show in the action bar.
			getMenuInflater().inflate(R.menu.course_browser, menu);
			restoreActionBar();
			return true;
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_go_back_home) {
			Intent intent = new Intent(this, HomeActivity.class);
			startActivity(intent);
			try {
				zip.close();                 // Conserve memory, release resources
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		if (id == R.id.action_play_lesson) {
			playLesson();
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void previewLesson(int lessonindex){
		selectedLesson = lessonindex;
		try {
			System.out.println(lessonindex);
			JSONObject thisLesson = lessons.getJSONObject(lessonindex);

			String lessonImPath = thisLesson.getString("image");
			   String lessonString = getString(R.string.lesson);
		        lessonString = lessonString.concat(" ").concat(String.valueOf((selectedLesson+1)));
		        		

	        setTitle(lessonString);
	        		
	        ZipEntry imentry = zip.getEntry(lessonImPath);
	        
	        InputStream input = zip.getInputStream(imentry);
	        
	        
	        input.close();
			
			
		} catch (JSONException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public void playLesson(){
		
	   	 Intent intent = new Intent(this, ItemPlayerActivity.class);
	  
	   	 intent.putExtra("selected", selectedLesson);
	        
	        startActivity(intent);
	}
	
	public void toggleExtra(View image){
		//PreviewFrag preview = (PreviewFrag)getSupportFragmentManager().findFragmentById(R.id.prev_lesson_frag);
	   // preview.toggleDescription();
	}
	
	
}
