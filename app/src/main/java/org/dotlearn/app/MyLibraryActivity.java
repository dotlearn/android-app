package org.dotlearn.app;

import java.io.IOException;


import java.util.ArrayList;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;



import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import org.dotlearn.app.models.*;
import org.dotlearn.app.util.*;

//import com.lamerman.FileDialog;

public class MyLibraryActivity extends ListActivity {
	
	public static final int REQUEST_SAVE = 1;
	public static final int REQUEST_LOAD = 2;
	public static final int BUTTON_NEUTRAL = -3;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		

		JSONArray lessons = null;
		try {

			LessonDB db = new LessonDB(this);

			lessons = db.getLessons();

			Intent intent = getIntent();

			if(lessons.length() == 0){
				if(intent.hasExtra("error")){

					startHomeActivity();

				} else{

					startWebStoreActivity();
				}
			} else{

				MyLibraryListAdapter adapter;


				if(intent.hasExtra("download")){
					String id = intent.getStringExtra("download");
					adapter = new MyLibraryListAdapter(this, lessons, id);
				} else{
					adapter = new MyLibraryListAdapter(this, lessons);
				}

				setListAdapter(adapter);
			}





		} catch (IOException | JSONException e) {
			e.printStackTrace();

			onError("Load Error", "There was an error loading the lesson: "+ e.getMessage());
		}


     
	}

	
	public void startHomeActivity(){


		
		Intent intent = new Intent(getBaseContext(), org.dotlearn.app.HomeActivity.class);
        startActivityForResult(intent, REQUEST_SAVE);
		
	}
	
	public void downloadFile(){
		
	}
	
	// My List Adapter
	// In the list adapter,
	  // You 
	/*
	 * 
	 * In the list adapter, the onlick listener calls the downloadfunction (with the view)
	 * The whole view needs to be edited in the download function
	 * Then needs to be sent
	 * 
	 * How to disable others?
	 *  Hold a disabled in MyLibrary
	 *  Otherwise, 
	 *  How to handle multiple Downloads?
	 *  
	 *  Well, what if the download just affects that individual screen?
	 *  
	 */
	
	
	public void startFileSearchActivity (){
	
		/*Intent intent = new Intent(getBaseContext(), com.lamerman.FileDialog.class);
		// In the future, let's create a folder: dotLearn/
        intent.putExtra(FileDialog.START_PATH,  Environment.getExternalStorageDirectory().getPath());
        
        //can user select directories or not
        intent.putExtra(FileDialog.CAN_SELECT_DIR, false);
        
        //alternatively you can set file filter
        intent.putExtra(FileDialog.FORMAT_FILTER, new String[] { "zip", "lsn", "crs" });
        
        startActivityForResult(intent, REQUEST_SAVE);
*/
	}
	
	/*
    public synchronized void onActivityResult(final int requestCode,
            int resultCode, final Intent data) {

            if (resultCode == Activity.RESULT_OK) {

                    if (requestCode == REQUEST_SAVE) {
                            System.out.println("Saving...");
                    } else if (requestCode == REQUEST_LOAD) {
                            System.out.println("Loading...");
                    }
                    
                    String filePath = data.getStringExtra(FileDialog.RESULT_PATH);
                    System.out.println(filePath);
                    
                    importFile(filePath);

            } else if (resultCode == Activity.RESULT_CANCELED) {

            }

    }

    
    public void importFile(String input){
 
    	final String filePath = input;
    	
		final Global global = ((Global)this.getApplication());
		
   	      final  Intent intent = new Intent(this, org.dotlearn.app.MyLibraryActivity.class);
   	      
   	      final Context context = getBaseContext();
    	
        new Thread(new Runnable() { 
        	
        	public void run(){
	    
                try {
    	
                	JSONdb db = new JSONdb(context);
                	
        			FileUtil.move(filePath, db.storageFolder);
        			
        			
        			String[] filePathParts = filePath.split("/");
        			String fileName = filePathParts[filePathParts.length -1];
        			

        			global.importFile(fileName);
        	         startActivity(intent);
        			
        			
        			
        		} catch (Exception e) {
        			// TODO Auto-generated catch block
        			e.printStackTrace();
        		}
        		

        		
        	}
        }).start();

    
    	
    	
    }
	
 */

	  @Override
	  protected void onListItemClick (ListView l, View v, int position, long id) {
		  
		  

		  try {
			  LessonDB db = new LessonDB(this);

			  JSONArray lessons = db.getLessons();

			  JSONObject lesson = lessons.getJSONObject(position);
			  String lessonLocation = lesson.optString("id") + ".lsn";

			  ((Global)this.getApplication()).setCourseFile(lessonLocation);
			   Intent intent = new Intent(this, org.dotlearn.app.FilePreviewActivity.class);
			 ((Global)this.getApplication()).setCourseFile(lessonLocation);
			 startActivity(intent);

			
		} catch (JSONException | IOException e) {
			// TODO Auto-generated catch block
				e.printStackTrace();
			  onError("Load Error", "There was an error loading the lesson: "+ e.getMessage());
		}

	  }
	  

	   public void startFilePreviewActivity(){
	    	 Intent intent = new Intent(this, org.dotlearn.app.FilePreviewActivity.class);
	         startActivity(intent);
	    }
		
		@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
			
		getMenuInflater().inflate(R.menu.course_library, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int id = item.getItemId();
		if (id == R.id.action_ecourse_store) {		
			startWebStoreActivity();
		}

		
		if (id == R.id.action_settings) {		
			Intent intent = new Intent(this, org.dotlearn.app.SettingsActivity.class);
			startActivity(intent);
		}
		
		if (id == R.id.action_about) {
			Intent intent = new Intent(this, org.dotlearn.app.AboutActivity.class);
			startActivity(intent);
		}
		return super.onOptionsItemSelected(item);
	}
	
	
	public void startWebStoreActivity (){
		Intent intent = new Intent(getBaseContext(), org.dotlearn.app.WaitingActivity.class);
        startActivityForResult(intent, REQUEST_SAVE);

	}



	public  void onError(String title, String message){



		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);


		final Context mcontext  = this;

		// set title
		alertDialogBuilder.setTitle(title);

		// set dialog message
		alertDialogBuilder.setMessage(message);

		alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				//do things

				Intent intent = new Intent(mcontext, org.dotlearn.app.MyLibraryActivity.class);
				intent.putExtra("error", true);
				startActivity(intent);

			}
		});


		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();

	}
	

}
