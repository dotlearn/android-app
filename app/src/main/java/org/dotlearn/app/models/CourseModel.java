package org.dotlearn.app.models;

public class CourseModel {
	

    private String impath;
    private String title;
    private String author;
    private String subject;
 
    private boolean isGroupHeader = false;
 
    public CourseModel(String title) {
        this("",title,null, "subject");
        isGroupHeader = true;
    }
    public CourseModel(String impath, String title, String author, String subject) {
        super();
        this.impath = impath;
        this.title = title;
        this.author = author;
        this.subject = subject.replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\"", "");
        

        
        
    }
	public String getIcon() {
		// TODO Auto-generated method stub
		//return icon;
		return impath;
		
	}
	public String getTitle() {
		// TODO Auto-generated method stub
		return title;
	}
	public String getAuthor() {
		// TODO Auto-generated method stub
		return author;
	}
	
	public String getSubject(){
		return subject;
	}

}
