package org.dotlearn.app.models;

import org.dotlearn.app.R;
import org.dotlearn.app.util.ImageDB;
import org.dotlearn.app.util.WebImageView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import android.widget.RelativeLayout;
import android.widget.TextView;

public class CategoryGridAdapter extends BaseAdapter {
	
	
   private JSONArray array;
   private Context mContext;
	private  JSONTree tree;


	private ImageDB db;

	float d;
	
   
   public CategoryGridAdapter(Context c, JSONArray inputArray, JSONTree tree, ImageDB db) {
       this.mContext = c;
       this.array = inputArray;
	   this.tree = tree;
	   this.db = db;



	  this.d = mContext.getResources().getDisplayMetrics().density;
   }
   

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return array.length();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		
		try {
			return array.get(arg0);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup root) {
		// TODO Auto-generated method stub
		
		
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


		RelativeLayout button = (RelativeLayout) inflater.inflate(R.layout.node_menu, root, false);




		try {

			final JSONObject child = array.getJSONObject(position);    //Get the child

			if(child.getString("type").compareTo("node")==0){    //If it's a node



				setupNode(button, child);




			} else{                                               // If it's a lesson

				button = (RelativeLayout) inflater.inflate(R.layout.lesson_library, root, false);

				setupLesson(button, child);

			}




		} catch (JSONException e) {
			e.printStackTrace();
		}





		return button;
	}



	public  void setupLesson(RelativeLayout button, JSONObject child) throws  JSONException {


		String name = child.getString("name");
		String color = color = child.optString("color", "none");
		String im = child.optString("image", "none");
		String id = child.getString("id");



		// Get Items
		WebImageView image = (WebImageView) button.findViewById(R.id.LibPreviewImg);
		TextView title = (TextView) button.findViewById(R.id.LessonTitle);
		TextView itemsCount = (TextView) button.findViewById(R.id.LessonItems);
		TextView fileSizeView = (TextView) button.findViewById(R.id.LessonFileSize);




		//Set the item count
		JSONArray items = child.getJSONArray("items");
		itemsCount.setText(String.valueOf(items.length()));


		title.setText(name);



		int fileSize = child.getInt("size");
		int  mb =(1024*1024);

		float fileSizeMB  = (float) (Math.round((((float)fileSize)/((float)(mb))*100))/100.0);

		String fileSizeString =  String.valueOf(fileSizeMB) + " MB";
		fileSizeView.setText(fileSizeString);




		//--------------------- Put the color ---------------------------
		if (!(color.compareTo("none") == 0)) {  // Color

			if ((color.compareTo("inherit") == 0)) {
				image.setBackgroundColor(Color.parseColor("#ffffff"));
			} else {
				image.setBackgroundColor(Color.parseColor(color));
			}

		}

		// Image Stuff

		RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) image.getLayoutParams();

		if (im.compareTo("none") == 0) {  // No Image
			rlp.width = (int) (d * 15);
		} else {


			String imgURL = mContext.getString(R.string.server_images).concat(im);
			image.setImageUrl(imgURL, db);







		}

	}

	public void setupNode(RelativeLayout button, JSONObject child) throws  JSONException{


		String name = child.getString("name");
		String color = color = child.optString("color", "none");
		String im = child.optString("image", "none");

		String id = child.getString("id");




		// Get Items
		final WebImageView image = (WebImageView) button.findViewById(R.id.web_icon);
		TextView title = (TextView) button.findViewById(R.id.item_title);



		title.setText(name);



		if(!(color.compareTo("none")==0)){  // Color

			if((color.compareTo("inherit")==0)){
				image.setBackgroundColor(Color.parseColor("#ffffff"));
			}else{
				image.setBackgroundColor(Color.parseColor(color));
			}

		}

		RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) image.getLayoutParams();


		System.out.println("Image: " + im);
		if(im.compareTo("none")==0){  // No Image

			rlp.width = (int)(d*15);  //15dp with

		} else{                       // Image

			if(im.compareTo("inherit")==0){                     // Hide inherited images
				rlp.width = (int)(d*15);  //15dp with

				System.out.println(im);
			} else{                                           // Show
				rlp.width = (int)(d*60);  //15dp with
				final String imgURL = mContext.getString(R.string.server_images).concat(im);


				image.setImageUrl(imgURL, db);

			}


		}


	}






}
