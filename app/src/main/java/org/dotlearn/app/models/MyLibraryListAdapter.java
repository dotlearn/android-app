package org.dotlearn.app.models;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.dotlearn.app.R;
import org.dotlearn.app.R.drawable;
import org.dotlearn.app.R.id;
import org.dotlearn.app.R.layout;
import org.dotlearn.app.util.Callback;
import org.dotlearn.app.util.JSONdb;
import org.dotlearn.app.util.LessonDB;
import org.dotlearn.app.util.WebImageView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;


import android.app.DownloadManager;
import android.app.DownloadManager.Request;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;

public class MyLibraryListAdapter extends BaseAdapter{
	
	   private Context context;
       private JSONArray lessons;
       private String toDownload = "";
  
       
       public MyLibraryListAdapter(Context context, JSONArray lessons) {
    	   
           this.context = context;
           this.lessons = lessons;
  
       }
       
       public MyLibraryListAdapter(Context context, JSONArray lessons, String toDownload) {
    	   
           this.context = context;
           this.lessons = lessons;
           this.toDownload = toDownload;
           
           
  
       }
       
       
       public View getView(int position, View convertView, ViewGroup parent) {
    	   
    

               // 1. Create inflater 
               LayoutInflater inflater = (LayoutInflater) context
                   .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

               // 2. Get rowView from inflater

               final View rowView = inflater.inflate(R.layout.lesson, parent, false);
               
                   
                   try {
					final JSONObject lesson = lessons.getJSONObject(position);
					
				    TextView titleView = (TextView) rowView.findViewById(R.id.item_title);
					titleView.setText(lesson.optString("name"));
					
					TextView subjectView =(TextView)rowView.findViewById(R.id.item_subject);
					
					
					
					if(!lesson.optBoolean("downloaded", false)){
						rowView.setBackgroundColor(Color.parseColor("#E5E5E5"));
					    
						ImageButton downloadButton  = (ImageButton) rowView.findViewById(R.id.DownloadButton);
					    downloadButton.setVisibility(View.VISIBLE);
					    
					    WebImageView image = (WebImageView) rowView.findViewById(R.id.web_icon);
					    
					    image.setVisibility(View.VISIBLE);
					    


						System.out.println(lesson.toString());
					   
						String imageURL = getString(R.string.server_images).concat(lesson.getString("image"));

						System.out.println(imageURL);

		
				        image.setImageUrl(imageURL);
				        image.setScaleType(ImageView.ScaleType.CENTER_CROP);      
				     //   image.setPlaceholderImage(R.drawable.gray_logo);
				        
				        
					      int fileSize = lesson.optInt("filesize");
						    int  mb =(1024*1024);
						    
						    float fileSizeMB  = (float) (Math.round((((float)fileSize)/((float)(mb))*100))/100.0);
						
						    String fileSizeString =  String.valueOf(fileSizeMB) + " MB";
						    
						    subjectView.setText(fileSizeString);
						

						    
						    if(lesson.optString("id").compareTo(toDownload)==0){
						    	downloadLesson(lesson, rowView);	
						    } else{
						    	
							    downloadButton.setOnClickListener(new Button.OnClickListener(){
									@Override
									public void onClick(View v) {
										downloadLesson(lesson, rowView);			
									}
							    });
						    	
						    	
						    }
						    

					    
					    
					
					} else{
						
						ImageView image = (ImageView)rowView.findViewById(R.id.item_icon);
						image.setVisibility(View.VISIBLE);
						
						String location = lesson.optString("id").concat(".lsn");
						
						
						System.out.println(lesson.toString());
						
						String category = lesson.optString("category");
					    subjectView.setText(category);
					    
					    
						ImageView sprite = (ImageView)rowView.findViewById(R.id.exercises);
						sprite.setVisibility(View.VISIBLE);
					    
						TextView itemsCount = (TextView) rowView.findViewById(R.id.LessonItems);
						itemsCount.setVisibility(View.VISIBLE);
						
						
						try {
							
							
							
							CourseFile file = new CourseFile(location, context);
				            image.setImageBitmap(file.getImage());	
				            
				            System.out.println(lesson.toString());
				            JSONArray items = lesson.getJSONArray("items");
							
				            itemsCount.setText(String.valueOf(items.length()));
							
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						
						
					}
					
					
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
                   

           return rowView;
       }

       
    public void downloadLesson(final JSONObject lesson, final View rowView){
    	
    	System.out.println(lesson.optString("id"));
    	
    	ImageButton downloadButton  = (ImageButton) rowView.findViewById(R.id.DownloadButton);
	    downloadButton.setVisibility(View.INVISIBLE);
	    
		final TextView subjectView =(TextView)rowView.findViewById(R.id.item_subject);
		subjectView.setText("Downloading...");
    	
		
		final ProgressBar progressBar = (ProgressBar) rowView.findViewById(R.id.progressBar);
		
		progressBar.setVisibility(View.VISIBLE);
		
	    final WebImageView webImage = (WebImageView) rowView.findViewById(R.id.web_icon);
		final ImageView image = (ImageView)rowView.findViewById(R.id.item_icon);
		final TextView progressText = (TextView)rowView.findViewById(R.id.item_dl_progress);
		
		JSONdb dbManager = new JSONdb(context);
		
		File folder = new File(dbManager.storageFolder);
		
		if(!folder.exists()){
			folder.mkdirs();
		}
		
		
	
		System.out.println("Starting download");
		
		String destination = folder.getAbsolutePath().concat("/").concat(lesson.optString("id")).concat(".lsn");
		
		File file = new File(destination);
		


		Uri imgUri = Uri.fromFile(file);
		
		String  downloadUrl =  getString(R.string.server_files).concat(lesson.optString("id")).concat(".lsn");

		
		 final DownloadManager dm = (DownloadManager) context.getSystemService(context.DOWNLOAD_SERVICE);
		    Request request = new Request(Uri.parse(downloadUrl));
		    request.setDestinationUri(imgUri);
		    
		    final long downloadId = dm.enqueue(request);
		
		    
			  
			 BroadcastReceiver onComplete=new BroadcastReceiver() {
	     		    public void onReceive(Context ctxt, Intent intent) {
	     		    long reference = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
	     		    	
	     		    
	     		    if(downloadId == reference){
	     		    	

	     				rowView.setBackgroundColor(Color.parseColor("#FFFFFF"));
	     				progressBar.setVisibility(View.GONE);
	     				subjectView.setText(lesson.optString("category"));
	     				
	     				try {
							lesson.put("downloaded", true);
							LessonDB db = new LessonDB(context);
							db.putLesson(lesson);
							String location = lesson.optString("id").concat(".lsn");
							
						    CourseFile file = new CourseFile(location, context);
					        image.setImageBitmap(file.getImage());	
							webImage.setVisibility(View.INVISIBLE);	
							image.setVisibility(View.VISIBLE);	
							progressText.setVisibility(View.INVISIBLE);
							
							ImageView sprite = (ImageView)rowView.findViewById(R.id.exercises);
							sprite.setVisibility(View.VISIBLE);
						    
							TextView itemsCount = (TextView) rowView.findViewById(R.id.LessonItems);
							itemsCount.setVisibility(View.VISIBLE);
							
							JSONArray items = lesson.getJSONArray("items");
							
							itemsCount.setText(String.valueOf(items.length()));
							
							
								
							
						} catch (JSONException | IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
	     				

	     		    }
	     		    }
	     		    
			 };
			 
			 
			 
			    new Thread(new Runnable() {

		            @Override
		            public void run() {


		                boolean downloading = true;
		                
		                

		                while (downloading) {

		                    DownloadManager.Query q = new DownloadManager.Query();
		                    q.setFilterById(downloadId);

		                    Cursor cursor = dm.query(q);
		                    cursor.moveToFirst();
		                    int bytes_downloaded = cursor.getInt(cursor
		                            .getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
		                    int bytes_total = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));
		                    
		                    int  mb =(1024*1024);

		                    if (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_SUCCESSFUL) {
		                        downloading = false;
		                    }

		                    final int dl_progress = (int) ((bytes_downloaded * 100l) / bytes_total);
		                    
						    float downloaded  = (float) (Math.round((((float)bytes_downloaded)/((float)(mb))*100))/100.0);
						    
						    float total = (float) (Math.round((((float)bytes_total)/((float)(mb))*100))/100.0);
							
						    final String  fileSizeString =  String.valueOf(downloaded)+ "/"+String.valueOf(total) + " MB";
		       
			 
		                    
		                    
		                    ((android.app.Activity)context).runOnUiThread(new Runnable() {

		                        @Override
		                        public void run() {
		                          progressText.setText(fileSizeString);
		                           progressBar.setProgress((int) dl_progress);

		                        }
		                    });


		              
		                    cursor.close();
		         
		                }
		                
		            }
		            
			    }).start();
		                
		            
			 
			 
			 
			 context.registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

    	
    	
    	
    	
    }

    public String getString(int id){
    	
    	return context.getString(id);
    }
       
       
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return lessons.length();
	}


	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return lessons.optJSONObject(arg0);
	}


	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

       
       
	
	
	/*


    // 3. Get icon,title & author views from the rowView
    
  //  context.getResources().getDrawable(R.id.item_icon);
    
    
    
    ImageView imgView = (ImageView) rowView.findViewById(R.id.item_icon); 

    TextView authorView = (TextView) rowView.findViewById(R.id.item_author);
    TextView subjectView = (TextView) rowView.findViewById(R.id.item_subject);

    // 4. Set the text for textView 
    
		               
     String impath = modelsArrayList.get(position).getIcon();
		          System.out.println(impath);
		               
		   			     File sdCard = Environment.getExternalStorageDirectory();
		   			     String filePath = sdCard.getAbsolutePath() + "/dotLearn/images/" + impath;
		   			     
		   			     File imFile = new File(filePath);
		   			     
		   			  if(imFile.exists()){
		
		   			    Bitmap bm = BitmapFactory.decodeFile(imFile.getAbsolutePath());
		
		   			    imgView.setImageBitmap(bm);
		
		   			}  else{
		   				
		   		        imgView.setImageResource(R.drawable.gray_logo);
		   				
		   			}
		   			  
		   			


    titleView.setText(modelsArrayList.get(position).getTitle());
    authorView.setText(modelsArrayList.get(position).getAuthor());
    subjectView.setText(modelsArrayList.get(position).getSubject());
    
    
 //   if(modelsArrayList.isEmpty()){
 	   
 	//   rowView = inflater.inflate(R.layout, parent, false);
   // }
    
//    } else{
	   
	   
//       }
    
    
    */

}
