package org.dotlearn.app.models;

import java.io.IOException;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;


import java.io.File;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.commons.io.IOUtils;

import org.dotlearn.app.util.FileFormatException;
import org.dotlearn.app.util.JSONdb;
import org.dotlearn.app.util.LrnFileException;
import org.json.JSONException;
import org.json.JSONObject;


public class CourseFile {
	
	
	
	public String type;
	public String location;
	public JSONObject json;
	private Context context;
    
	public String databaseFolder;
	
	private String imageFolder = "meta/";
	
	public ZipFile zip;
	
	private String currentItem;
	
	
	
	public CourseFile(String location, Context context) throws IOException{
		
		
		JSONdb db = new JSONdb(context);
		
		databaseFolder = db.storageFolder;
		
		
		
		//Step 1
		checkFileType(location);
		
		//Step 2
		checkCourseFile(location);
		
		this.context = context;
		

		
		
	}
	
	
	public void delete() throws IOException, JSONException{
		
		String id = json.getString("id");
		
		
		JSONdb manager = new JSONdb(context);
		
		 manager.init();
		 
		 JSONObject db = manager.get();

		
		JSONObject filedata = db.getJSONObject(id);
		
	    String location = filedata.getString("location");
	    
	   String filePath = databaseFolder.concat(location);
	   
	   File file = new File (filePath);
	   
	   file.delete();
	   
	   db.remove(id);
	   
	   manager.save();
	   
	   
		
		
	}
	
	
	
	public void setCurrentItem(String id){
		currentItem = id;
	}
	
	
	
	public void checkFileType(String location) throws FileFormatException{
		

		if(location.contains(".")){
			
			String[] URIparts = location.split(Pattern.quote("."));
			String extension = URIparts[URIparts.length -1];
			
			
			System.out.println("The file location is "+location);
			System.out.println(URIparts.length);
			
			switch(extension){
		     case "lsn":
			      this.type = "lesson";
			      break;
		     case "crs":
		    	  this.type = "course";
		    	  break;
		     default:
			 		String [] accepted = new String[2];
					accepted[0] = "lsn";
					accepted[1] = "crs";
		    	  throw new FileFormatException(extension, accepted);	
		}
		
			
			
		} else{
	 		String [] accepted = new String[2];
			accepted[0] = "lsn";
			accepted[1] = "crs";
			throw new FileFormatException("none", accepted);
		}
		




      
	}

 
	   public Bitmap getImage () throws JSONException, IOException{
		   
		   
		    String impath = imageFolder.concat(json.getString("image"));

	        ZipEntry imentry = zip.getEntry(impath);
	        
	        InputStream input = zip.getInputStream(imentry);
	        
	        Bitmap image = BitmapFactory.decodeStream(input);
	        
	        input.close();
	
		   
		return image;
		   
		   
	   }
	   
	   
	   
	   public CoursePreview  getCoursePreview() throws JSONException, IOException{
		   	
	 		Bitmap image = this.getImage();
		    CoursePreview preview = new CoursePreview(json, image, context);
		  
		return preview;
		   
	   }
	   
	   public InputStream getData(String filename) throws IOException{
		   
            String filepath = "res/".concat(currentItem).concat("/").concat(filename);
            System.out.println(filepath);
            
            ZipEntry entry = zip.getEntry(filepath);
            
		   return  zip.getInputStream(entry);
	   }
	   
	   public InputStream getMetaData(String filename) throws IOException{
		   
           String filepath = "meta/".concat(filename);
           
           ZipEntry entry = zip.getEntry(filepath);
           
		   return  zip.getInputStream(entry);
	   }
	
	   public void checkCourseFile(String filename) throws IOException {
	    	
		   
		   System.out.println(filename);
		   System.out.println(databaseFolder);
		   
		   String filepath = databaseFolder.concat(filename);
		   System.out.println(filepath);

	    	ZipEntry coursefile = null;
	    	
			
	    	try {
				zip = new ZipFile(filepath);
				
			     
				switch(type){
				case "lesson":
					coursefile = zip.getEntry("lesson.json");
					break;
				case "course":
					 coursefile = zip.getEntry("course.json");
					 break;
				
				}
				
				

	
               if (coursefile == null){
            	   System.out.println(type);
			 //	   zip.close();
            	   throw new LrnFileException("Coursefile not found");
               }
			 	
	 		
			 		InputStream input = zip.getInputStream(coursefile);
			 		
			 		StringWriter writer = new StringWriter();
			 		IOUtils.copy(input, writer, "UTF-8");
			 		String jsonstring = writer.toString();
			 		
			 		input.close();

			 	    json = new JSONObject(jsonstring); 
			 
			 		
			 		Boolean name = json.has("name");
			 		Boolean description = json.has("description");
			 		Boolean id = json.has("id");
			 		
		 		
                    if(name&&description&&id){
                    	
                  
                    } else{
                 	   throw new LrnFileException("Badly formed coursefile - required fields missing");
                    }
			 		

		
			} catch (JSONException e) {

				e.printStackTrace();             // JSON formatting issues
                throw new LrnFileException("JSON Error: Badly formed .lrn file");
	 
			}	
	    	
	    }


}

