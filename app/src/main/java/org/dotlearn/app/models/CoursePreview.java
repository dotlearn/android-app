package org.dotlearn.app.models;

import org.dotlearn.app.R;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Bitmap;

public class CoursePreview {

	public String name;
	public String description;
	public Bitmap image;
	public String id;
	
	public String author;
	public String subauthor;
	public String language;
	public String copyright;
	public String subjects;
	public String prereqs;
	public Context context;
	public JSONObject lesson;
	public ItemAdapter items;
	
	public CoursePreview(JSONObject lesson, Bitmap image, Context context){
		
		this.context = context;
		this.name = lesson.optString("name");
		this.description = lesson.optString("description");
		this.image = image;
		
		this.author = lesson.optString("author");
		
		
		String subjects = lesson.optString("subjects").replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\"", ""); 
		
		this.subauthor = getString(R.string.author).concat(":").concat(lesson.optString("author"));
		this.language = getString(R.string.language).concat(":").concat(lesson.optString("language"));
		this.copyright = getString(R.string.copyright).concat(":").concat(lesson.optString("copyright"));
		this.subjects = getString(R.string.subjects).concat(":").concat(subjects);
		this.prereqs = getString(R.string.prereqs).concat(":").concat(lesson.optString("prereqs", getString(R.string.none)));
		this.id = getString(R.string.id).concat(":").concat(lesson.optString("id"));
		
		JSONArray itemsArray = lesson.optJSONArray("items");
		
		this.items = new ItemAdapter(itemsArray, context);
		
	
		
		
		
			
	}
	
	
	public String getString(int id){
		
		return context.getString(id);
	}
	
	
}
