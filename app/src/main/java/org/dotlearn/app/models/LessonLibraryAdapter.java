package org.dotlearn.app.models;

import org.dotlearn.app.R;
import org.dotlearn.app.util.WebImageView;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;
import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class LessonLibraryAdapter extends BaseAdapter {
	
	 private JSONArray array;
	   private Context mContext;
		
	   
	   public LessonLibraryAdapter(Context c, JSONArray inputArray) {
	       this.mContext = c;
	       this.array = inputArray;
	   }
	   

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return array.length();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			
			try {
				return array.get(arg0);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}

		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup root) {
			// TODO Auto-generated method stub
			
			
	        LayoutInflater inflater = (LayoutInflater) mContext
	                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			
	        
	        
			RelativeLayout button = (RelativeLayout)inflater.inflate(R.layout.lesson_library, root, false);
			
			
			TextView titleView = (TextView) button.findViewById(R.id.LessonTitle);
			TextView fileSizeView = (TextView) button.findViewById(R.id.LessonFileSize);
			WebImageView image = (WebImageView) button.findViewById(R.id.LibPreviewImg);
			TextView itemsView = (TextView) button.findViewById(R.id.LessonItems);
		
		
			try {	
				
				JSONObject lesson = array.getJSONObject(position);
				
					System.out.println(lesson.getJSONArray("items").toString());
					
				    		
				    titleView.setText(lesson.getString("name"));
				    
				    JSONArray items = lesson.getJSONArray("items");
				    itemsView.setText(String.valueOf(items.length()));
				    
				    int fileSize = lesson.getInt("filesize");
				    int  mb =(1024*1024);
				    
				    float fileSizeMB  = (float) (Math.round((((float)fileSize)/((float)(mb))*100))/100.0);
				
				    String fileSizeString =  String.valueOf(fileSizeMB) + " MB";

				    fileSizeView.setText(fileSizeString);
				    
				    				    
					String imageURL = mContext.getString(R.string.server_images).concat(lesson.getString("image"));
				//	String imageURL = mContext.getString(R.string.server_default_image);
					
					System.out.println(imageURL);
			        image.setImageUrl(imageURL);
			        image.setScaleType(ImageView.ScaleType.CENTER_CROP);      
			        image.setPlaceholderImage(R.drawable.gray_logo);
			        
			        

			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			
				
			return button;
		}
	

	    
}


