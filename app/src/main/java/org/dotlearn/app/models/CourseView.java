package org.dotlearn.app.models;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.view.Gravity;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import org.dotlearn.app.R;
import org.dotlearn.app.util.WebImageView;

public class CourseView extends RelativeLayout {
	

	public CourseView(Context context) {
		super(context);
	}
	
	
	
	public void setMeta(JSONObject meta){           // I was having problems inflating a view, figure this out later
	

		
	      WebImageView image = new WebImageView(getContext());
		
          image.setScaleType(ImageView.ScaleType.CENTER_CROP);
      
          image.setPlaceholderImage(R.drawable.gray_logo);
          
          image.setMinimumWidth(180);
          image.setMinimumHeight(180);
          
  		
  	    	String url;
  		

            url = getString(R.string.server_images).concat(meta.optString("image"));

	  		
	  		System.out.println(url);
  
          image.setImageUrl(url);
          
          this.addView(image);
	
          LinearLayout layout = new LinearLayout(getContext());
          LayoutParams params =  new LayoutParams(180, 50);
          
    
          params.addRule(RelativeLayout.ALIGN_LEFT, image.getId());
          params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

       
          layout.setGravity(Gravity.CENTER); 
 
          params.setMargins(0, 0, 0, 0);
         
          TextView title = new TextView(getContext());
          title.setPadding(4, 8, 4, 8);

          title.setTextColor(getResources().getColor(R.color.light_text));
         
          title.setTextSize(12);
          title.setGravity(Gravity.CENTER);

          layout.setLayoutParams(params);
          layout.setBackgroundColor(getResources().getColor(R.color.dotLearn));
          title.setText(meta.optString("name", "course"));
          

          
          layout.addView(title);
          this.addView(layout);
          
	}
	
	
	
	
	public String getString(int id){
		
		return getContext().getString(id);
		
	}
	
	
	
	
	
	
	
	
	
	

	
	
}
