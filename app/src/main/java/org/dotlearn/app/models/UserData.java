package org.dotlearn.app.models;

import org.dotlearn.app.Global;
import org.json.JSONObject;

public class UserData {

	private Global global;
	
	private int item;
	
	
	public UserData(Global global, int item){
		 this.global = global;
		 this.item = item;
	}
	
	
	
	public JSONObject getUserData(){
		return global.getItemData(item-1);
	}
	
	
	public void setJSONObject(JSONObject x){
		global.setItemData(item-1, x);
	}
	
	
	
	
	
	
	
	
	
	
	

	
	
	
}
