package org.dotlearn.app.models;

import java.util.Hashtable;

import org.dotlearn.app.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ItemAdapter extends BaseAdapter  {
	
	private JSONArray items;
	private Context context;
	private Hashtable<String, Drawable> drawables;
	private Resources r;
	
	public ItemAdapter(JSONArray items, Context context){
		
		this.items = items;
		this.context = context;
		
        drawables = new Hashtable<String, Drawable>();
        
        
        r = context.getResources();
        /*
        drawables.put("media.first", r.getDrawable(R.drawable.sprite_media_first));
        drawables.put("media.mid", r.getDrawable(R.drawable.sprite_media_mid));
        drawables.put("media.last", r.getDrawable(R.drawable.sprite_media_last));
        drawables.put("media.standalone", r.getDrawable(R.drawable.sprite_media_standalone));
        drawables.put("exercise.first", r.getDrawable(R.drawable.sprite_exercise_first));
        drawables.put("exercise.mid", r.getDrawable(R.drawable.sprite_exercise_mid));
        drawables.put("exercise.last", r.getDrawable(R.drawable.sprite_exercise_last));
        drawables.put("exercise.standalone", r.getDrawable(R.drawable.sprite_exercise_standalone));
        drawables.put("doc.first", r.getDrawable(R.drawable.sprite_doc_first));
        drawables.put("doc.mid", r.getDrawable(R.drawable.sprite_doc_mid));
        drawables.put("doc.last", r.getDrawable(R.drawable.sprite_doc_last));
        drawables.put("doc.standalone", r.getDrawable(R.drawable.sprite_doc_standalone));
        
        */
	}
	

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return items.length();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View view, ViewGroup rootView) {
		// TODO Auto-generated method stub
		
		
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        
     	RelativeLayout rowView = (RelativeLayout)inflater.inflate(R.layout.item, rootView, false);
     	TextView label = (TextView)rowView.findViewById(R.id.Label);
		
	
     	ImageView sprite = (ImageView)rowView.findViewById(R.id.Sprite);
     		
        
        try {
			JSONObject item = items.getJSONObject(position);
			
			label.setText(item.optString("label"));
			
			String stage;
			String type;
			
			if(items.length()==1){
		        stage   = "standalone";
			} else{
				
				if(position == 0){
					stage = "first";
				} else if (position == items.length()-1){
					stage = "last";
				} else{
					stage = "mid";
				}
			}
			
			switch(item.optString("module")){
				case "video":
				case "audio":
				case "audio-slideshow":
					type = "media";
					break;
				case "image":
				case "html":
				case "pdf":
					type="doc";
					break;
				case "quiz-multiple":
				case "fill-in-the-blank":
				default: 
				    type = "exercise";
				    break;
			}
			
			
			Drawable d = null;
			
			String key  = type.concat(".").concat(stage);
			
			if(drawables.containsKey(key)){
				d = drawables.get(key);
			} else{
				
				switch(key){
				    case "media.first":
					    d = r.getDrawable(R.drawable.sprite_media_first);
					    break;
				    case "media.last":
					    d = r.getDrawable(R.drawable.sprite_media_last);
					    break;
				    case "media.mid":
					    d = r.getDrawable(R.drawable.sprite_media_mid);
					    break;
				    case "media.standalone":
					    d = r.getDrawable(R.drawable.sprite_media_standalone);
					    break;
				    case "doc.first":
					    d = r.getDrawable(R.drawable.sprite_doc_first);
					    break;
				    case "doc.last":
					    d = r.getDrawable(R.drawable.sprite_doc_last);
					    break;
				    case "doc.mid":
					    d = r.getDrawable(R.drawable.sprite_doc_mid);
					    break;
				    case "doc.standalone":
					    d = r.getDrawable(R.drawable.sprite_doc_standalone);
					    break;
				    case "exercise.first":
					    d = r.getDrawable(R.drawable.sprite_exercise_first);
					    break;
				    case "exercise.last":
					    d = r.getDrawable(R.drawable.sprite_exercise_last);
					    break;
				    case "exercise.mid":
					    d = r.getDrawable(R.drawable.sprite_exercise_mid);
					    break;
				    case "exercise.standalone":
					    d = r.getDrawable(R.drawable.sprite_exercise_standalone);
					    break;
				    
				}
				
				
				drawables.put(key, d);
				
			}
			
			sprite.setImageDrawable(d);
	
					
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
     
		
		
		return rowView;
	}

}
