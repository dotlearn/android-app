package org.dotlearn.app.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONTree {

	JSONObject tree;
	String selected = "0";



	public JSONTree(JSONArray array) throws JSONException{


		tree = new JSONObject();
		selected = "0";


		JSONArray categories = new JSONArray();


		for (int i = 0; i < array.length(); i++){


			JSONObject current = array.getJSONObject(i);
			String currentID = current.getString("id");
			tree.put(currentID, current);

			if(current.optString("parent").compareTo("0") == 0){
				//If parent is root node, then add to categories array
				categories.put(current);
			}

		}


		if(!tree.has("0")){

			// If the server doesn't supply a root

			JSONObject root = new JSONObject();
			root.put("name", "Library");
			root.put("type", "name");
			root.put("id", "0");
			root.put("children", categories);
			tree.put("0", root);

		}

	}



	public JSONTree(String input) throws JSONException{
		

		JSONArray array = new JSONArray(input);
		tree = new JSONObject();
		selected = "0";
		

		JSONArray categories = new JSONArray();
		
		
		for (int i = 0; i < array.length(); i++){
			
			
			JSONObject current = array.getJSONObject(i);
			String currentID = current.getString("id");
			tree.put(currentID, current);

			if(current.optString("parent").compareTo("0") == 0){
				//If parent is root node, then add to categories array
				categories.put(current);
			}
			
		}


		if(!tree.has("0")){

			// If the server doesn't supply a root

			JSONObject root = new JSONObject();
			root.put("name", "Library");
			root.put("type", "name");
			root.put("id", "0");
			root.put("children", categories);
			tree.put("0", root);

		}

	}
	
	
	
	
	
	public JSONObject getCurrent() throws JSONException{
		
		return tree.getJSONObject(selected);
		
	}
	
	
	public void setCurrent(String id){

		selected = id;
		
	}
	
	
	public void setCurrent(JSONObject current) throws JSONException{
		
	  selected = current.getString("id");
		
	}
	
	public JSONObject getById(String id) throws JSONException{
		
		return tree.getJSONObject(id);
		
	}
	
	public JSONArray getChildren() throws JSONException{
		
		//JSONObject current = getCurrent();
		
		
		return getChildren(selected);
		
	}
	
	public JSONArray getChildren(String id) throws JSONException{
		
		
		JSONObject current = getById(id);
		JSONArray childrenIDs = current.getJSONArray("children");
		
		JSONArray children = new JSONArray();
		
		for (int i=0; i < childrenIDs.length(); i++){
			
			JSONObject child = tree.getJSONObject(childrenIDs.getString(i));
			child.put("category", current.getString("name"));
			children.put(child);
		}
		
		
		System.out.println(children.toString());
		
		return children;
	
	
	}


	public String getInherited(String id, String field){

		String toReturn = "";

		try{
			JSONObject object = getById(id);

			String value = object.getString(field);

			if(value.compareTo("inherit")==0){  //If this level is asking an inherit, ask the parent

				if(!(id.compareTo("0") ==0)){            //Prevent climbing beyond root
					String parentId = object.getString("parent");
					toReturn = getInherited(parentId, field);
				}

			}else {                          //If this level has the value, return it
				toReturn = value;
			}

		} catch (JSONException e){

			e.printStackTrace();
		}


		return  toReturn;
	}



	
	
	
	
	
	
	

	
	
	
	
	
	
	
}
