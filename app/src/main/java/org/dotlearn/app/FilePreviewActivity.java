package org.dotlearn.app;

import java.io.File;
import java.io.IOException;

import org.dotlearn.app.models.CourseFile;
import org.dotlearn.app.models.CoursePreview;
import org.dotlearn.app.util.JSONdb;
import org.json.JSONException;
import org.json.JSONObject;

import android.widget.Button;

import android.support.v7.app.ActionBarActivity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ImageView;



public class FilePreviewActivity extends ActionBarActivity {
		public String jsonCourse;
		public String zipPath;
		public JSONObject lesson;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_library_preview);  //Set the main view
		
      setCourseFilePreview();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.file_preview, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		int id = item.getItemId();
		
	
		if (id == R.id.action_play) {

			 playLesson();
		}
		

		return super.onOptionsItemSelected(item);
	}

	public void setCourseFilePreview(){
		
		final CourseFile file = ((Global)this.getApplication()).getCourseFile();
		lesson = file.json;

		new Thread( new Runnable() {
		    @Override
		    public void run() {
		    			
		    	try {
		    		
					final CoursePreview preview = file.getCoursePreview();
								
					runOnUiThread(new Runnable() {

                        @Override
                        public void run() {

                        	
           
       					 ((TextView) findViewById(R.id.LibPreviewTitle)).setText(preview.name);
    					 ((TextView) findViewById(R.id.LibPreviewDescription)).setText(preview.description);
    			         ((ImageView) findViewById(R.id.LibPreviewImg)).setImageBitmap(preview.image);
    			
    			         ((ImageButton) findViewById(R.id.InstalledPlayButton)).setVisibility(View.VISIBLE);
    			         ((ImageButton) findViewById(R.id.DeleteButton)).setVisibility(View.VISIBLE);
    			         
    			         ListView items = (ListView) findViewById(R.id.LessonItems);
    			         items.setAdapter(preview.items);
    			       

    			         

                        }
                    });
					
					
				} catch (JSONException | IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    	

		    	
		    }
		}).start();
	
		
	}
	


	public void deleteFile(View view){
		
		
		String id = lesson.optString("id");
		
	   JSONdb dbManager = new JSONdb(this);
	   
	   try {
		JSONObject db = dbManager.getDBFromFile();
		
		db.remove(id);
		
		dbManager.set(db);
		
		dbManager.save();
		
		String lessonLocation = dbManager.storageFolder + id + ".lsn";
		
		File lessonFile = new File(lessonLocation);
		
		if(lessonFile.exists()){
			lessonFile.delete();
		}
		
		
		Intent intent = new Intent(this, org.dotlearn.app.MyLibraryActivity.class);
	       startActivity(intent);
		

	} catch (IOException | JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	   
	   	
	}
	

	
	public void goPlay(View view){
		
		playLesson();
	}
	
	
	public void playLesson(){
	   	 Intent intent = new Intent(this, ItemPlayerActivity.class);	      	    
	     startActivity(intent);
		
	}

}
