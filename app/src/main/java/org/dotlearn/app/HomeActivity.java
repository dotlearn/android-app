package org.dotlearn.app;



import android.support.v7.app.ActionBarActivity;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
//import com.lamerman.*;


public class HomeActivity extends ActionBarActivity {
	
	
	public static final int REQUEST_SAVE = 1;
	public static final int REQUEST_LOAD = 2;
	public static final int BUTTON_NEUTRAL = -3;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
		setContentView(R.layout.activity_home);         //Set the default content
	}

	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		
		/*
		if (id == R.id.action_search_files) {
//			viewLibrary();
			startFileSearchActivity();
		}*/
		if (id == R.id.action_about) {
			Intent intent = new Intent(this, org.dotlearn.app.AboutActivity.class);
			startActivity(intent);
		}
		
		if (id == R.id.action_ecourse_store) {
			go2WebStoreActivity();
		}
		if (id == R.id.action_settings) {
			Intent intent = new Intent(this, org.dotlearn.app.SettingsActivity.class);
			startActivity(intent);
		}
		return super.onOptionsItemSelected(item);
	}

	
	/*
	public void startFileSearchActivity (){
		Intent intent = new Intent(getBaseContext(), com.lamerman.FileDialog.class);
		// In the future, let's create a folder: dotLearn/
        intent.putExtra(FileDialog.START_PATH,  Environment.getExternalStorageDirectory().getPath());
        
        //can user select directories or not
        intent.putExtra(FileDialog.CAN_SELECT_DIR, false);
        
        //alternatively you can set file filter
        intent.putExtra(FileDialog.FORMAT_FILTER, new String[] { "zip", "lsn", "crs" });
        
        startActivityForResult(intent, REQUEST_SAVE);

	}*/
	
	public void goLibrary(View view){
		go2WebStoreActivity();
	}
	
	
	public void go2WebStoreActivity (){
		Intent intent = new Intent(getBaseContext(), org.dotlearn.app.WaitingActivity.class);
        startActivityForResult(intent, REQUEST_SAVE);
	}
	
	public void go2WebStoreActivity (View view){
             go2WebStoreActivity();
	}
	
	
	public void viewLibrary (){
		Intent intent = new Intent(getBaseContext(), org.dotlearn.app.MyLibraryActivity.class);
		// In the future, let's create a folder: dotLearn
        startActivityForResult(intent, REQUEST_SAVE);

	}
	
	/*
    public synchronized void onActivityResult(final int requestCode,
            int resultCode, final Intent data) {

            if (resultCode == Activity.RESULT_OK) {

                    if (requestCode == REQUEST_SAVE) {
                            System.out.println("Saving...");
                    } else if (requestCode == REQUEST_LOAD) {
                            System.out.println("Loading...");
                    }
                    
                    String filePath = data.getStringExtra(FileDialog.RESULT_PATH);
                    System.out.println(filePath);

            } else if (resultCode == Activity.RESULT_CANCELED) {

            }

    }
	*/

}


 