package org.dotlearn.app.util;
import java.io.IOException;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;


public class LessonDB {

	private Context mContext;
	JSONdb dbManager;
	JSONObject db;

	public LessonDB(Context context) throws IOException, JSONException {
		
		this.mContext = context;

		dbManager = new JSONdb(mContext);


		db = dbManager.getDBFromFile();

	}
	
	
	
	public void putLesson(JSONObject lesson){
		

        
		System.out.println("Saving to database");
        
        try {

			JSONObject lessons  = new JSONObject();

			if(db.has("lessons")){
				lessons = db.optJSONObject("lessons");
			} else {
				System.out.println("No lessons item in db, adding new");
			}

			  // Get Lessons from Master DB file

			String fileLocation = lesson.optString("id").concat(".lsn");
			String imageLocation = lesson.optString("image");
			
            JSONObject document = new JSONObject();
            
            
    		document.put("id", lesson.opt("id"));
			document.put("name", lesson.opt("name"));
			document.put("description", lesson.opt("description"));
			document.put("author", lesson.opt("author"));
			document.put("copyright", lesson.opt("copyright"));
			document.put("language", lesson.opt("language"));
			document.put("subjects", lesson.opt("subjects"));
			document.put("filesize", lesson.opt("filesize"));
			document.put("category", lesson.opt("category"));
			document.put("items", lesson.opt("items"));
			document.put("downloaded", lesson.opt("downloaded"));
			document.put("location", fileLocation);
			document.put("pre-requisites", lesson.opt("pre-requisites"));
			document.put("image", imageLocation);
			

			
			lessons.put(lesson.optString("id"), document);                  //Put new document in the lessons object
			db.put("lessons", lessons);                                     // Put lessons back in master db object


			dbManager.set(db);                                              //Save master db object
			
			dbManager.save();

			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        
		
		
	}




	public JSONObject getLessonIndex() throws IOException, JSONException {

		JSONObject toReturn = new JSONObject();


			if(db.has("lessons")){
				toReturn = db.getJSONObject("lessons");
			}



		return toReturn;

	}


	public JSONArray getLessons() throws IOException, JSONException {
		
		JSONArray toReturn = new JSONArray();
		


		    if(db.has("lessons")){

				JSONObject lessons = db.getJSONObject("lessons");

				Iterator<?> keys = lessons.keys();

				while( keys.hasNext() ){
					String key = (String)keys.next();
					if( lessons.get(key) instanceof JSONObject ){

						toReturn.put(lessons.get(key));

					}
				}
			}

		


		return toReturn;
		
	}
	
	
	

}
