package org.dotlearn.app.util;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

import java.io.OutputStreamWriter;

import org.dotlearn.app.R;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.Environment;

public class JSONdb {
	
	public String folderName;
	
	public  String dbName;
	public  String dbLocation;
	public String storageFolder;
	public  File dbFile;
	
	
	
	public static File sdCard = Environment.getExternalStorageDirectory();
	

	
	
	public Context mContext;
	
	public JSONObject dbObject;
	
	public JSONdb(Context mContext){
		
		this.mContext = mContext;
		folderName = "dotlearn";
		dbName = "db.json";

		storageFolder = sdCard.getAbsolutePath().concat("/").concat(folderName).concat("/");
		dbLocation = storageFolder.concat(dbName);
		dbFile = new File(dbLocation);
	
		
	}
	
	
	
	public JSONObject getDBFromFile() throws IOException, JSONException{
		
		JSONObject toReturn = new JSONObject();
		
		
		if(dbFile.exists()){
			
		   System.out.println("Loading Database");
	
		  StringBuilder text = new StringBuilder();	  
			
		  FileReader reader = new FileReader(dbFile);
		  
		  BufferedReader bReader = new BufferedReader(reader);
		  
		  String line;
		  
		  
		    while ((line = bReader.readLine()) != null) {
		        text.append(line);
		        text.append('\n');
		    }
		    
		    
		  
		  
		   toReturn = new JSONObject(text.toString());
		  
			
			
		} else{
			
		  System.out.println("Creating new Database File");
		  
		  toReturn = newDBFile();
		  
		  
		}
		
		
		return toReturn;
		
	}
	
	
	public JSONObject newDBFile() throws IOException{
		
		File folder = new File(storageFolder);
		
		if(!folder.exists()){
			folder.mkdirs();
		}
		
		if(!dbFile.exists()){
			dbFile.createNewFile();
		}
		

		
		dbObject = new JSONObject();
		save();
		return dbObject;
		
	}
	
	
	
	public void init(){
		
		try {
	
			dbObject = getDBFromFile();
			System.out.println("Database Loaded");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			
			System.out.println("IO Exception loading the database");
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			System.out.println("JSON exception loading the database");
		}
	}
	
	
	
	
	public JSONObject get(){
		
		return dbObject;
		
		
	}
	
	public void set(JSONObject object){
		
		dbObject = object;
		
	}
	
	
	public void save() throws IOException{
		
		
		if(!dbFile.exists()){
			  dbObject = newDBFile();
		}
		
		FileOutputStream f = new FileOutputStream(dbFile);
		
		OutputStreamWriter osw = new OutputStreamWriter(f);
		
		osw.append(dbObject.toString());

		osw.close();
		
		f.close();
		
	}
	
	

}
