package org.dotlearn.app.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;


import android.os.AsyncTask;
import android.util.Log;




public class FetchJSON extends AsyncTask<String, Void, String>{

	public org.dotlearn.app.util.AsyncResponse delegate;
	

	 public FetchJSON(org.dotlearn.app.util.AsyncResponse delegate){
	        this.delegate=delegate;
	    }
	

	   
	    protected String doInBackground(String... address) {
	    	
	    	
	    	
	    	StringBuilder builder = new StringBuilder();
	    	HttpClient client = new DefaultHttpClient();
	    	HttpGet httpGet = new HttpGet(address[0]);
	    	
	    	
	        try {
	        	
	      		HttpResponse response = client.execute(httpGet);
	    		StatusLine statusLine = response.getStatusLine();
	    		int statusCode = statusLine.getStatusCode();
	    		if(statusCode == 200){
	    			HttpEntity entity = response.getEntity();
	    			InputStream content = entity.getContent();
	    			BufferedReader reader = new BufferedReader(new InputStreamReader(content));
	    			String line;
	    			while((line = reader.readLine()) != null){
	    				builder.append(line);
	    			}
	    		} else {
	    		//	Log.e(Library.class.toString(),"Failedet JSON object");
	    		}
	    		
	    		
	    	}catch(ClientProtocolException e){
	    		e.printStackTrace();
	    	} catch (IOException e){
	    		e.printStackTrace();
	    	}
	    	return builder.toString();
	    }
	        	
	        	
	    @Override
	    protected void onPostExecute(String response) {
	        // TODO: check this.exception 
	        // TODO: do something with the feed
	    	
	    	 delegate.processFinish(response);
	    }


	
}