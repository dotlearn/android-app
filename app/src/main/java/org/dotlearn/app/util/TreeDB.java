package org.dotlearn.app.util;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Iterator;

/**
 * Created by sam on 12/26/15.
 */
public class TreeDB {


        private Context mContext;

        public TreeDB(Context context){

            this.mContext = context;
        }



        public void putTree(JSONArray tree){

            JSONdb dbManager = new JSONdb(mContext);

            System.out.println("Saving to database");

            try {
                JSONObject db = dbManager.getDBFromFile();    //Master dB file

                db.put("tree", tree);

                dbManager.set(db);                                              //Save master db object
                dbManager.save();


            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


        }



        public JSONArray getTree(){

            JSONArray toReturn = new JSONArray();

            JSONdb dbManager = new JSONdb(mContext);


            try {
                JSONObject db = dbManager.getDBFromFile();

                toReturn = db.getJSONArray("tree");


            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


            return toReturn;

        }






}
