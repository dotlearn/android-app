package org.dotlearn.app.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;

import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;

import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;

public class POSTFetchJSON extends AsyncTask<String, Void, String>{

	public org.dotlearn.app.util.AsyncResponse delegate;
	
	private JSONObject jsonObject;

	

	 public POSTFetchJSON(org.dotlearn.app.util.AsyncResponse delegate){
	        this.delegate=delegate;
	    }
	
	 
	    public void setData(String inputJSON) throws JSONException{
	    	
	    	jsonObject = new JSONObject(inputJSON);
	    }
	    
	    public void setData(JSONObject inputJSON){
	    	
	    	jsonObject = inputJSON;
	    }

	   
	    protected String doInBackground(String... address) {
	    	
	    	StringBuilder builder = new StringBuilder();
	    	HttpClient client = new DefaultHttpClient();	    	
	    	HttpPost httppost = new HttpPost(address[0]);
	    	
	        try {
	        	 
	        	   StringEntity se = new StringEntity(jsonObject.toString());
	        	   se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
	        	
	        	   httppost.setEntity(se);  
	        	
	      		HttpResponse response = client.execute(httppost);
	    		StatusLine statusLine = response.getStatusLine();
	    		
	    		int statusCode = statusLine.getStatusCode();
	    		if(statusCode == 200){
	    			HttpEntity entity = response.getEntity();
	    			InputStream content = entity.getContent();
	    			BufferedReader reader = new BufferedReader(new InputStreamReader(content));
	    			String line;
	    			while((line = reader.readLine()) != null){
	    				builder.append(line);
	    			}
	    		} else {
	    			
	    			builder.append("A Problem occurred: status code....");
	    			builder.append(statusCode);
	    	
	    		}
	    		
	    		
	    	}catch(ClientProtocolException e){
	    		e.printStackTrace();
	    	} catch (IOException e){
	    		e.printStackTrace();
	    	}
	    	return builder.toString();
	    }
	        	
	        	
	    @Override
	    protected void onPostExecute(String response) {
	    	 delegate.processFinish(response);
	    }


	
}
