package org.dotlearn.app.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.regex.Pattern;


/**
 * Created by sam on 12/23/15.
 */
public class ImageDB {


    Context mContext;
    String folderName = "webimages/";
    String imagesFolder;

    JSONdb db;

    JSONObject cached;
    JSONObject dbObject;

    public ImageDB(Context context) throws IOException, JSONException {

        this.mContext = context;
        db = new JSONdb(context);
        imagesFolder = db.storageFolder.concat(folderName);

        dbObject = db.getDBFromFile();


     //   cached = dbObject.optJSONObject("cachedImages");



        if(dbObject.has("cachedImages")){
            cached = dbObject.getJSONObject("cachedImages");
        } else{
            cached = new JSONObject();
        }


    }



    public Boolean hasImage(String url){


        Boolean response = false;



       if(cached != null){
           response = cached.has(getFileName(url));
       }


        return response;
    }

    public  String getFileName(String url){
        String[] parts = url.split(Pattern.quote("/")); // Split on period

        String filename = parts[parts.length -1];

        return  filename;
    }

    public void putImage(String url, byte[] data) throws IOException, JSONException {




        System.out.println("Saving image " + url);

        String filename = getFileName(url);

        String filepath = imagesFolder.concat(filename);


        registerImage(filename, filepath);


        System.out.println("Saving image " + url + " at filepath " + filepath);



        FileUtils.writeByteArrayToFile(new File(filepath), data);


        System.out.println(filename + " saved successfully");


    }


    public String getFileLocation(String url) throws IOException, JSONException {


//
  //      JSONObject dbObject = db.getDBFromFile();
    //    JSONObject cached = dbObject.getJSONObject("cachedImages");


        String filename = getFileName(url);


        return  cached.getString(filename);
    }



    public  void registerImage(String filename, String filepath) throws IOException, JSONException {
/*

        JSONObject dbObject = db.getDBFromFile();


        JSONObject cached = new JSONObject();

        if(dbObject.has("cachedImages")){

            cached = dbObject.getJSONObject("cachedImages");
        }

*/

        cached.put(filename, filepath);




        dbObject.put("cachedImages", cached);


        db.set(dbObject);

        db.save();

    }


    public Bitmap getImage(String url) throws IOException, JSONException {


        System.out.println("Getting string url" + url);

        String path = getFileLocation(url);

        System.out.println("Path: " +path);

        Bitmap bitmap = BitmapFactory.decodeFile(path);


        return  bitmap;
    }


}
