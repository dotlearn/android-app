package org.dotlearn.app.util;

import java.io.IOException;

public class LrnFileException extends IOException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public String msg;
	
	public LrnFileException(String msg){
		
		this.msg = msg;
	}
	
}
