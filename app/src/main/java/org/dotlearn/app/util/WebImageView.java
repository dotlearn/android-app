package org.dotlearn.app.util;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONException;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.widget.ImageView;

public class WebImageView extends ImageView  {



	ImageDB db;

	Context mContext;
	
	 private Drawable placeholder, image;

	    public WebImageView(Context context) {
	        super(context);
			mContext = context;

	    }
	    
	    public WebImageView(Context context, AttributeSet attrs, int defStyle) {
	        super(context, attrs, defStyle);
			mContext = context;

	    }
	    public WebImageView(Context context, AttributeSet attrs) {
	        super(context, attrs);
			mContext = context;

	    }

	    public void setPlaceholderImage(Drawable drawable) {
	        placeholder = drawable;
	        if (image == null) {
	            setImageDrawable(placeholder);
	        }
	    }
	    public void setPlaceholderImage(int resid) {
	        placeholder = getResources().getDrawable(resid);
	        if (image == null) {
	            setImageDrawable(placeholder);
	        }
	    }

	    public void setImageUrl(String url) {

			LoadImage task = new LoadImage();
			task.execute(url);

	    }

		public  void setImageUrl(String url, ImageDB db){



			if(db != null){
				this.db = db;

				if(db.hasImage(url)){
					LoadImageTask task = new LoadImageTask();
					task.execute(url);
				}else{
					DownloadTask task = new DownloadTask();
					task.execute(url);
				}
			} else{

				LoadImage task = new LoadImage();
				task.execute(url);


			}




		}




		private class LoadImage extends  AsyncTask<String, Void, Boolean>{


			private String url;

			@Override
			protected  Boolean doInBackground (String... params){


				Boolean exists = false;

				try {
					db = new ImageDB(mContext);
					this.url = params[0];
					exists = db.hasImage(this.url);
				} catch (JSONException | IOException e) {
					e.printStackTrace();
				}


				return  exists;
			}



			@Override
			protected void onPostExecute(Boolean exists) {

				if(exists){
					LoadImageTask task = new LoadImageTask();
					task.execute(url);
				}else{
					DownloadTask task = new DownloadTask();
					task.execute(url);
				}
			}


		}




	    private class DownloadTask extends AsyncTask<String, Void, Bitmap> {

	        @Override
	        protected Bitmap doInBackground(String... params) {
	            String url = params[0];
	            try {
	                URLConnection conn = (new URL(url)).openConnection();
	                InputStream is = conn.getInputStream();
	                BufferedInputStream bis = new BufferedInputStream(is);

	                ByteArrayBuffer baf = new ByteArrayBuffer(50); 
	                int current = 0;
	                while ((current=bis.read()) != -1) {
	                    baf.append((byte)current);
	                }

	                byte[] imageData = baf.toByteArray();


					System.out.println("Successfully downloaded image, saving");
					db.putImage(url, imageData);


	                return BitmapFactory.decodeByteArray(imageData, 0, imageData.length);

	            } catch (Exception e) {


					System.out.println("An error ocurred while saving the image the image");
					e.printStackTrace();


	                return null;
	            }
	        }

	        @Override
	        protected void onPostExecute(Bitmap result) {
	            image = new BitmapDrawable(result);
	            if (image != null) {
	                setImageDrawable(image);
	            }
	        }
	    }




	private class LoadImageTask extends AsyncTask<String, Void, Drawable> {

		@Override
		protected Drawable doInBackground(String... params) {
			String url = params[0];

			Drawable toReturn = null;

			try {

				System.out.println("Starting Async for " + url);
				Bitmap image = db.getImage(url);
				System.out.println("Setting image to cached");
				System.out.println(image.toString());
				Drawable imageDrawable = new BitmapDrawable(image);

				System.out.println(imageDrawable.toString());

				toReturn = imageDrawable;


			} catch (IOException | JSONException e) {
				e.printStackTrace();

				/*
				System.out.println("Error retrieving cached - downloading fresh");
				DownloadTask task = new DownloadTask();
				task.execute(url);*/
			}

			return toReturn;
		}




		@Override
		protected void onPostExecute(Drawable image) {

			if (image != null) {
				setImageDrawable(image);
			}
		}
	}
	
	
	
}
