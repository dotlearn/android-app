package org.dotlearn.app.util;

public interface AsyncResponse {

	 void processFinish(String output);
	
}
