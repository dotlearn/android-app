package org.dotlearn.app.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;



public class FileUtil {

	
	public static void move(String input, String dest) throws Exception{
		
    	
    	File inputFile = new File(input);
    	
    	if(!inputFile.exists()){
    		throw new Exception();
    	}
    	
    	
    	
    	String[] fileNameParts = input.split("/");
    	if(fileNameParts.length < 1){		
    		throw new Exception();
    	}    	 
    	
    	String fileName = fileNameParts[fileNameParts.length -1];
    	
    	
    	
    	File destFile = new File(dest);
    	
    	
    	if(!destFile.exists()){
    		
    		
    	
    	
        if(destFile.mkdirs()){
        	
        	if(destFile.isDirectory()){

        		destFile = new File(dest.concat(fileName));
        	}
        	
        	
        } else{
        	
        	if(!destFile.createNewFile()){
        		throw new Exception();
        		
      
        	}
        	
        }
        
    	} else{
    		
           	if(destFile.isDirectory()){

        		destFile = new File(dest.concat(fileName));
        	}
        	
    		
    	}

    	
    	InputStream in = new FileInputStream(inputFile);
    	OutputStream out = new FileOutputStream(destFile);

    	// Transfer bytes from in to out
    	byte[] buf = new byte[1024];
    	int len;
    	while ((len = in.read(buf)) > 0) {
    	    out.write(buf, 0, len);
    	}
    	in.close();
    	out.close();
    	
    	
  
    	inputFile.delete();
    	
    	
    	
	}
}
