package org.dotlearn.app.modules;

import java.io.File;

import java.io.BufferedOutputStream;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;
import org.dotlearn.app.models.CourseFile;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.MediaController;
import android.widget.VideoView;

public class VideoModule extends DataModule {
	
	VideoView videoView;

	public VideoModule(Context context, ViewGroup root, CourseFile file) {
		super(context, root, file);
		// TODO Auto-generated constructor stub
		
		videoView = new VideoView(context);
		
		
		videoView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		this.setView(videoView);

		
		
	}
	
	
	public void onLoad(String json){
		
		JSONObject data;
		try {
			data = new JSONObject(json);
			
		//	videoView = new VideoView(mcontext);

			
			MediaController mc = new MediaController(mcontext);
			mc.show(0);
			videoView.setMediaController(mc);
			
			
			System.out.println(data.getString("location"));
			InputStream videoStream = file.getData(data.getString("location"));
			 
			new createVideo().execute(videoStream);
	        
		} catch (JSONException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private class createVideo extends AsyncTask<InputStream, Void, File> {
	    /** The system calls this to perform work in a worker thread and
	      * delivers it the parameters given to AsyncTask.execute()  */
	    protected File doInBackground(InputStream... streams) {
		

	    File videofile = new File("tmp.mp4");
	    
	    try{
	    
			if (videofile.exists()){
				videofile.delete();
			}
			
		videofile = File.createTempFile("tmp", "mp4");
		InputStream input = streams[0];
		OutputStream out =  new BufferedOutputStream(new FileOutputStream(videofile));
		
		byte[] bytes = IOUtils.toByteArray(input);
		
		input.close();
		out.write(bytes);
		out.flush();
		out.close();	
	    }
	    
	    catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
			
	    return videofile;
	    	
	    }
	    
	    /** The system calls this to perform work in the UI thread and delivers
	      * the result from doInBackground() */
	    protected void onPostExecute(File videoFile) {
	       
			Uri uri = Uri.fromFile(videoFile);
			videoView.setVideoURI(uri);
	
		

			videoView.start();
	    	
	    }


	}
	
	@Override
	public void onNext(){
		
		if(videoView.isPlaying()){
		videoView.stopPlayback();
		}
	}
	

}

/*		
videoView.setOnCompletionListener(new OnCompletionListener(){
	public void onCompletion(MediaPlayer mp) {
         
		if (currentItem == items.length()){
		} else {
			final Handler handler = new Handler();
			handler.postDelayed(new Runnable() {
			  @Override
			  public void run() {
					itembox.removeViewAt(0);
					setItem(currentItem + 1);
			  }
			}, 1500);

		}
	}
});

*/	
