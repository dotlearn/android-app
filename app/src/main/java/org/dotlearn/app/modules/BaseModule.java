package org.dotlearn.app.modules;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class BaseModule {
	
	public View view;
	public ViewGroup root;
	public LayoutInflater inflater;
    public Object r;
    public Context mcontext; 
	
	
	public BaseModule(Context context, ViewGroup root){	
		view = new View(context);
		this.root = root;
	    
		this.mcontext = context;
	

		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	public void setView(View view){
		this.view = view;
	}
	
	
	
	
	public void onInit(){
		
	}
	
	public void onLoad(String data){
		
		
	}
	
	
	public void onNext(){
		
		
	}
	
	public View onLessonEnd(){
		
		return null;
		
	}
	
	public View onLessonEnd(String data){
		
		return null;
		
	}
	
	
	
	

}
