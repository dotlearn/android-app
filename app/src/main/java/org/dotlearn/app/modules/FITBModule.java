package org.dotlearn.app.modules;

import org.dotlearn.app.Global;
import org.dotlearn.app.R;
import org.dotlearn.app.models.UserData;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ImageView;

public class FITBModule extends UserDataModule {

	public FITBModule(Context context, ViewGroup root, Global global) {
		super(context, root, global);
		// TODO Auto-generated constructor stub
		

	      RelativeLayout layout = (RelativeLayout) inflater.inflate(R.layout.item_fitb, root, false);
	      this.setView(layout);
	}
	
	@Override
	public void onLoad(String json, final UserData user){
		
		RelativeLayout layout = (RelativeLayout) this.view;
		try{
			
		     JSONObject data = new JSONObject(json);
		    ((TextView)layout.findViewById(R.id.prompt)).setText(data.optString("prompt"));
			
	 		final EditText editor = (EditText)layout.findViewById(R.id.edit);
	 		
	 		JSONObject userData = user.getUserData();
	 		
	 		String response = userData.optString("response");
	 		System.out.println(userData.toString());
	 		editor.setText(response);

	 	
	 		editor.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View arg0) {		
					editor.setInputType(InputType.TYPE_CLASS_TEXT);
				}
	 		});
	 		

	 		editor.setOnEditorActionListener(new EditText.OnEditorActionListener(){
				@Override
				public boolean onEditorAction(TextView arg0, int actionId, KeyEvent event) {
					
					if (actionId == EditorInfo.IME_ACTION_SEARCH ||
				            actionId == EditorInfo.IME_ACTION_DONE ||
				            event.getAction() == KeyEvent.ACTION_DOWN &&
				            event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
				        	
				        	JSONObject x = new JSONObject();
				        	
				        	try {
								x.put("response", editor.getText());
								System.out.println(x.toString());
								user.setJSONObject(x);
							//	global.setItemData(currentItem-1, x);
								editor.setInputType(InputType.TYPE_NULL);
								
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
				           return true; // consume.
				  //      }                

					} 
					// TODO Auto-generated method stub
					return false;
				}
	 			
	 		});

			
			
		//itembox.addView(layout);
			
			
			
			} catch(JSONException e){
				
				e.printStackTrace();
			}
			
		
	}
	
	@Override
	public View onLessonEnd(String json, UserData user){
		
		
	     LinearLayout quizResult = (LinearLayout) inflater.inflate(R.layout.lesson_end, root, false);	
	
  try {
	
		JSONObject data = new JSONObject(json);
		
		JSONArray answers = data.getJSONArray("answers");
		
		JSONObject userData = user.getUserData();
		 
		String response = userData.optString("response", "No Response Given");
	
		
	((TextView)quizResult.findViewById(R.id.prompt)).setText(data.optString("prompt"));
 	 

 	 String answerText = "Corect Answer: " + answers.getJSONObject(0).getString("text");
 
  	 ((TextView) quizResult.findViewById(R.id.correct)).setText(answerText);
 	
 	 
   Boolean correct = false;
   
   for(int i=0; i<answers.length(); i++){
   	
   	String answer = answers.getJSONObject(i).getString("text");

   	if(response.compareTo(answer) == 0){		
   		correct = true;
   	} 	
   }
   
   if(correct){

   	((ImageView) quizResult.findViewById(R.id.correctIcon)).setVisibility(View.VISIBLE);
   } else{
   	((ImageView) quizResult.findViewById(R.id.wrongIcon)).setVisibility(View.VISIBLE);
   }
   

 	 String responseText = "Your Answer: " + response;

	 	 ((TextView) quizResult.findViewById(R.id.user)).setText(responseText);
	 ((TextView) quizResult.findViewById(R.id.explanation)).setText(data.optString("explanation", "No explanation given"));	 


		 
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
	 
		 return quizResult;
	}

}
