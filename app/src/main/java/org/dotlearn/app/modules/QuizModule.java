package org.dotlearn.app.modules;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ImageView;

import org.dotlearn.app.Global;
import org.dotlearn.app.R;
import org.dotlearn.app.models.UserData;

import android.widget.TextView;

public class QuizModule extends UserDataModule {


	public QuizModule(Context context, ViewGroup root, Global global) {
		super(context, root, global);
		// TODO Auto-generated constructor stub
		
		
        LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.item_quiz, root, false);
        this.setView(layout);
	
	}
	
	@Override
	public void onLoad(String json, final UserData user){
		
		try {
			

	 		JSONObject quiz = new JSONObject(json);
	
	 		LinearLayout layout= (LinearLayout) this.view;
	 		
	 		
	 		TextView promptView = (TextView) layout.findViewById(R.id.prompt);
	 		
	 		
	 	    promptView.setText(quiz.optString("prompt")); 		
	 		
	 		JSONArray options = quiz.getJSONArray("options");
	 
	 

	     	JSONObject userData = user.getUserData();
		    int response = userData.optInt("response", -1);
	 		

	 	
	 		final RadioGroup radioGroup = (RadioGroup) layout.findViewById(R.id.radio);
	 		
	 		radioGroup.removeAllViews();
	 		
	 		
	 			final RadioButton[] option = new RadioButton[options.length()];
	 		
	 			int checked = -1;
	 			
	 			for (int i =0; i<options.length(); i++){
	 				option[i] = new RadioButton(mcontext);
	 				option[i].setTextColor(mcontext.getResources().getColor(R.color.darker));
	 				option[i].setText(options.getJSONObject(i).optString("text"));
	 				radioGroup.addView(option[i]);
	 			}
	 			
					radioGroup.check(checked);
	 			
	 			RadioGroup.OnCheckedChangeListener listener = new RadioGroup.OnCheckedChangeListener() {
					
					@Override
					public void onCheckedChanged(RadioGroup group, int checkedId) {
			
				     View radioButton = group.findViewById(checkedId);
				     int index = group.indexOfChild(radioButton);
		
				     JSONObject userData = new JSONObject();
				     try {
						userData.put("response", index);
						user.setJSONObject(userData);
						//  ((Global) mcontext.getApplication()).setItemData(currentItem-1, userData);
						
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					}
				};
	
				
	 			radioGroup.setOnCheckedChangeListener(listener);
	 	
 				
 		     	if (response > -1){		
					    	checked = option[response].getId();					    
				} 
 			
 				radioGroup.check(checked);
	
	 		
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
		
		
	}
	
	@Override
	public View onLessonEnd(String json, UserData user){
		

	     LinearLayout quizResult = (LinearLayout) inflater.inflate(R.layout.lesson_end, root, false);	
	
    try {

	
		JSONObject data = new JSONObject(json);
		
		int answer = data.getInt("answer");
		
		JSONObject userData = user.getUserData();
		 
		int response = userData.getInt("response");
	
	
   	  ((TextView)quizResult.findViewById(R.id.prompt)).setText(data.optString("prompt"));
   	 
	    
	    if(response == answer){
	    	((ImageView) quizResult.findViewById(R.id.correctIcon)).setVisibility(View.VISIBLE);
	    } else{
	    	
	    	((ImageView) quizResult.findViewById(R.id.wrongIcon)).setVisibility(View.VISIBLE);
	    }
	    
   	 String answerText = "Corect Answer: ";	
   	 JSONArray options = data.getJSONArray("options");
   	 JSONObject correctOption = options.getJSONObject(answer);	 
   	 answerText = answerText.concat(correctOption.optString("text", ""));
   	 ((TextView) quizResult.findViewById(R.id.correct)).setText(answerText);

   	 String responseText = "Your Answer: ";
    	 if (response > -1){
    	   	 JSONObject chosenOption = options.getJSONObject(response);  
       	     responseText = responseText.concat(chosenOption.optString("text"));	 
    	 } else{   		 
    		 responseText = responseText.concat("none");		 
    	 }
    	 
   
  	 	 ((TextView) quizResult.findViewById(R.id.user)).setText(responseText);
	 	 ((TextView) quizResult.findViewById(R.id.explanation)).setText(data.optString("explanation", "No explanation given"));	 

	 
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 
		 return quizResult;
	
		
	}

	
}
