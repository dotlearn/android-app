package org.dotlearn.app.modules;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;
import org.dotlearn.app.R;
import org.dotlearn.app.models.CourseFile;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff.Mode;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

public class AudioModule extends DataModule {
	public MediaPlayer audioPlayer;
	public LinearLayout audioView;
	public int audioCurrent;
	public Thread audioMonitor;
    public final Context thisContext= this.mcontext;
    public boolean ready;
	
	public AudioModule(Context context, ViewGroup root, CourseFile file) {
		super(context, root, file);
		
       audioView =    (LinearLayout) inflater.inflate(R.layout.item_audio, root, false);
        this.setView(audioView);
        
        audioPlayer = new MediaPlayer();
   
	}
	
	
	
	public void onLoad(String json){
		
		JSONObject data;
		try {
			data = new JSONObject(json);

			SeekBar x = (SeekBar) audioView.findViewById(R.id.audio_progress);
		
			x.getProgressDrawable().setColorFilter(Color.WHITE, Mode.SRC_IN);
			
			ImageView back = (ImageView) audioView.findViewById(R.id.audio_back);
			ImageView play = (ImageView) audioView.findViewById(R.id.audio_play);
			ImageView pause = (ImageView) audioView.findViewById(R.id.audio_pause);
			ImageView rewind = (ImageView) audioView.findViewById(R.id.audio_rewind);
			
			TextView caption = (TextView) audioView.findViewById(R.id.caption);
			
			ImageView icon = (ImageView)audioView.findViewById(R.id.audio_icon);
			
			
			 String itemPath = data.getString("image");
			 InputStream input = file.getData(itemPath);
			 Bitmap imageItem = BitmapFactory.decodeStream(input);
			
			 icon.setImageBitmap(imageItem);
			 
			 caption.setText(data.optString("caption"));
			
			LinearLayout audioControls = (LinearLayout)audioView.findViewById(R.id.audio_controls);
			ImageView audioIcon = (ImageView) audioView.findViewById(R.id.audio_icon);
			ProgressBar loading = (ProgressBar) audioView.findViewById(R.id.loading_icon);
			
		    x.setVisibility(View.GONE);
		    audioIcon.setVisibility(View.GONE);
		    loading.setVisibility(View.VISIBLE);
		    audioControls.setVisibility(View.GONE);
			
			SeekBar.OnSeekBarChangeListener l = new SeekBar.OnSeekBarChangeListener() {
				
				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress,
						boolean fromUser) {
				
                     if(fromUser){
  
                   
        
                    	int toSeekto = 	(int) ((audioPlayer.getDuration()*progress)/100);
                    	
                    	
                    	
                    	audioPlayer.seekTo(toSeekto);

              
                     
                     }
					
				}
			};
			x.setOnSeekBarChangeListener(l);
			

	            
	            
			back.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {
				
					audioPlayer.seekTo(0);
					audioProgress();
				}
			});
			
			play.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {
					

					
					audioPlayer.start();
					audioProgress();

					ImageView pause = (ImageView) audioView.findViewById(R.id.audio_pause);
					ImageView play = (ImageView) audioView.findViewById(R.id.audio_play);
					play.setVisibility(View.GONE);
					pause.setVisibility(View.VISIBLE);
					
				}
			});
			
			pause.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {
					
					audioCurrent = audioPlayer.getCurrentPosition();
					audioPlayer.pause();
					System.out.println("On Puase:");
					System.out.println(audioCurrent);
					ImageView pause = (ImageView) audioView.findViewById(R.id.audio_pause);
					ImageView play = (ImageView) audioView.findViewById(R.id.audio_play);
					play.setVisibility(View.VISIBLE);
					pause.setVisibility(View.GONE);
					
				}
			});
	
			
			
			rewind.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {
					int progress = audioPlayer.getCurrentPosition();
					int howMuchToGoBack = 15000;
					
					audioPlayer.pause();
					

					
					audioPlayer.start();
					
					if (progress < howMuchToGoBack){
						audioPlayer.seekTo(0);
					} else{
						audioPlayer.seekTo(progress - howMuchToGoBack);
					}
					
					audioProgress();
					
				}
			});
			


	        
	        InputStream audioStream = file.getData(data.getString("location"));
	        
	        new createAudio().execute(audioStream);
	        
		} catch (JSONException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	

	private class createAudio extends AsyncTask<InputStream, Void, File> {
	    /** The system calls this to perform work in a worker thread and
	      * delivers it the parameters given to AsyncTask.execute() **/
	    protected File doInBackground(InputStream... streams) {
		
	   InputStream input = streams[0];

	    File audiofile = new File("tmp.mp3");
	    
	    try{
	    
			if (audiofile.exists()){
				audiofile.delete();
			}
			
		audiofile = File.createTempFile("tmp", "mp3");

		OutputStream out =  new BufferedOutputStream(new FileOutputStream(audiofile));
		
		byte[] bytes = IOUtils.toByteArray(input);
		
		input.close();
		out.write(bytes);
		out.flush();
		out.close();	
	    }
	    
	    catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
			
	    return audiofile;
	    	
	    }
	    
	    /** The system calls this to perform work in the UI thread and delivers
	      * the result from doInBackground()   **/
	    protected void onPostExecute(File audioFile) {
	       
			Uri uri = Uri.fromFile(audioFile);
			audioPlayer = new MediaPlayer();

	        try {
				audioPlayer.setDataSource(thisContext, uri);
			

		        audioPlayer.prepare();
		        audioPlayer.start();
		        
		        
				SeekBar x = (SeekBar) audioView.findViewById(R.id.audio_progress);		
				ImageView audioIcon = (ImageView) audioView.findViewById(R.id.audio_icon);
				ProgressBar loading = (ProgressBar) audioView.findViewById(R.id.loading_icon);
				LinearLayout audioControls = (LinearLayout)audioView.findViewById(R.id.audio_controls);
				
			    x.setVisibility(View.VISIBLE);
			    audioIcon.setVisibility(View.VISIBLE);
			    loading.setVisibility(View.GONE);
			    audioControls.setVisibility(View.VISIBLE);
		        
		        System.out.println("ready");
		        audioProgress();

     
		        
		        

			} catch (IllegalArgumentException | SecurityException
					| IllegalStateException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	    	
	    }


	}
	
	@Override
	public void onNext(){
		
		  try {
				if (audioPlayer.isPlaying()){
					audioPlayer.stop();
			//		audioPlayer.release();
				}
			  } catch (IllegalStateException e){
					e.printStackTrace();
			  }
		
	}
	
	
	public void audioProgress(){
   
		audioMonitor =  new Thread(new Runnable() { 
        	
        	public void run() throws IllegalStateException {
        		
       
        		while(audioPlayer.isPlaying()){
        			
        			
        			
        			if(audioPlayer.isPlaying()){
        			
        			int progress = audioPlayer.getCurrentPosition();
        			int duration = audioPlayer.getDuration();
        			
        			final int dl_progress = (int) (((double)progress*100.00)/ ((double)duration));
        			
        			if(thisContext instanceof Activity){
        		    Activity activity = (Activity)thisContext;
        		
        		    activity.runOnUiThread(new Runnable() {

	                        @Override
	                        public void run() {

	                          SeekBar x = (SeekBar) audioView.findViewById(R.id.audio_progress);
                           x.setProgress(dl_progress);
	                        }
	                    });
        			}
        			
        			}
        		}
           
        		
        	}
        });
		
		try {
		audioMonitor.start();
		} catch(IllegalStateException e){
			
		}
      
		
		
	}

}

