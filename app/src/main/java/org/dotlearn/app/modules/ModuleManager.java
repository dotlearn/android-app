package org.dotlearn.app.modules;

import org.dotlearn.app.Global;
import org.dotlearn.app.models.CourseFile;
import org.dotlearn.app.models.UserData;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

public class ModuleManager {
	
	AudioModule itemAudio;
	VideoModule itemVideo;
	HTMLModule itemHTML;
	ImageModule itemImage;
	QuizModule itemQuiz;
	FITBModule itemFITB;
	PDFModule itemPDF;
    BaseModule [] modules;
	ViewGroup root;
	CourseFile file;
	AudioSlideshow itemAudioSlideshow;
	
	
	public ModuleManager(Context context, ViewGroup root, Global global){
		
		this.root = root;
	       
		
		 file = global.getCourseFile();
		
		itemHTML = new HTMLModule(context, root);
		itemImage = new ImageModule(context, root, file);
		itemVideo = new VideoModule(context, root, file);
		itemAudio = new AudioModule(context, root, file);
		itemQuiz = new QuizModule(context, root, global);
		itemFITB = new FITBModule(context, root, global);
		itemPDF = new  PDFModule(context, root, file);
		itemAudioSlideshow  = new AudioSlideshow(context, root, file);
		

		setModules(itemHTML, itemImage, itemVideo, itemAudio, itemQuiz, itemFITB, itemPDF, itemAudioSlideshow);
		
		
		onInit();
	}
	
	
	
	public void setModules(BaseModule...baseModules ){
		
		modules = baseModules;
		
		
		
	}
	
	public void onInit(){
		
		for (int i= 0; i < modules.length; i++){
			
			modules[i].onInit();
			
			root.addView(modules[i].view);
			
			
		}
		
		hideAll();
		
		
	}
	
	public void hideAll(){
		for (int i= 0; i < modules.length; i++){			
			modules[i].view.setVisibility(View.GONE);
		}
	}
	
	
	
	
	public void onLoad(JSONObject item, UserData user){
		
		hideAll();
	
		BaseModule current = getModule(item.optString("module"));
	
		file.setCurrentItem(item.optString("id"));
	
		
		
		String data = (String)item.optString("data");

		
		if(current instanceof UserDataModule){
			
			((UserDataModule) current).onLoad(data, user);
		
			
		} else{
			
			current.onLoad(data);
		}
			
		current.view.setVisibility(View.VISIBLE);
		
		
	}
	
	
	public View onLessonEnd(String module, String data, UserData user){
		
		hideAll();
		
	
		BaseModule current = getModule(module);
		
		View view = null;
		
		if(current instanceof UserDataModule){
			
			
			view = ((UserDataModule) current).onLessonEnd(data, user);
		
			
		} else{
			
			view = current.onLessonEnd(data);
		}
			
		return view;
	}
	
	
	public void onNext(String module){
	
		getModule(module).onNext();	
		
	}
	
	
	public BaseModule getModule(String name){
		
		BaseModule current = itemHTML;
		
		switch(name){
		case "html":		
			current = itemHTML;
			break;
		case "image":
			current = itemImage;
			break;
		case "audio":
			current = itemAudio;
	
			break;
		case "video":
			current = itemVideo;
			break;
		case "quiz-multiple":
			current = itemQuiz;
	
	        break;
		case "fill-in-the-blank":
			current = itemFITB;
			break;
		case "pdf":
			current = itemPDF;

			break;
			
		case "audio-slideshow":
			current = itemAudioSlideshow;

			break;
		}
	
		
		return current;
		
		
		
	}
	
	
	
	

}
