package org.dotlearn.app.modules;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;
import org.dotlearn.app.R;
import org.dotlearn.app.models.CourseFile;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff.Mode;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

public class AudioSlideshow extends DataModule {
	
	public MediaPlayer audioPlayer;
	public LinearLayout audioView;
	public int audioCurrent;
	public Thread audioMonitor;
    public final Context thisContext= this.mcontext;
    public boolean ready;
    public int current;
    public int length;
    public JSONArray slides;
    public JSONObject slide;
	public ImageView slideImage; 
	public TextView slideNumber;
    
    // Controls
    
    ImageView back;
    ImageView play;
    ImageView pause;
    ImageView next;

	public AudioSlideshow(Context context, ViewGroup root, CourseFile file) {      //Setup
		super(context, root, file);
		
		
	       audioView =    (LinearLayout) inflater.inflate(R.layout.item_audio_slideshow, root, false);
	       audioView.setVisibility(View.VISIBLE);
	        this.setView(audioView);
	        
	        audioPlayer = new MediaPlayer();
	        
	        setup();
	}
	

	public void onLoad(String json){                                //Handles the data
		
		JSONObject data;
		try {
			data = new JSONObject(json);
			
			
			slides = data.getJSONArray("slides");
			length = slides.length();
			
			init();                                
	        
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
	}
	
	
	
	public void setup(){
	
		setupUI();
		attachListeners();
			
	}
	
	public void attachListeners(){

	    
		back.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {       //Back
				
				previousSlide();
			}
		});
		
		play.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {            // Play

				audioPlayer.start();
				audioProgress();

				ImageView pause = (ImageView) audioView.findViewById(R.id.audio_pause);
				ImageView play = (ImageView) audioView.findViewById(R.id.audio_play);
				play.setVisibility(View.GONE);
				pause.setVisibility(View.VISIBLE);
				                          
			}
		});
		
		pause.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {            //Pause
				               
				audioCurrent = audioPlayer.getCurrentPosition();
				audioPlayer.pause();
				System.out.println("On Puase:");
				System.out.println(audioCurrent);
				ImageView pause = (ImageView) audioView.findViewById(R.id.audio_pause);
				ImageView play = (ImageView) audioView.findViewById(R.id.audio_play);
				play.setVisibility(View.VISIBLE);
				pause.setVisibility(View.GONE);
				
			}
		});


		
		next.setOnClickListener(new OnClickListener(){          // Next
			@Override
			public void onClick(View v) {

				nextSlide();
	
			}
		});
	}
	
	
	public void setupUI(){
		SeekBar x = (SeekBar) audioView.findViewById(R.id.audio_progress);
		
		x.getProgressDrawable().setColorFilter(Color.WHITE, Mode.SRC_IN);
		
		back = (ImageView) audioView.findViewById(R.id.audio_back);
		play = (ImageView) audioView.findViewById(R.id.audio_play);
		pause = (ImageView) audioView.findViewById(R.id.audio_pause);
		next = (ImageView) audioView.findViewById(R.id.audio_next);              // Set this to next
		slideImage = (ImageView) audioView.findViewById(R.id.audio_icon);
		slideNumber = (TextView) audioView.findViewById(R.id.slideNumber);
		
		LinearLayout audioControls = (LinearLayout)audioView.findViewById(R.id.audio_controls);

		ProgressBar loading = (ProgressBar) audioView.findViewById(R.id.loading_icon);
		
	    x.setVisibility(View.VISIBLE);
	    slideImage.setVisibility(View.GONE);
	    loading.setVisibility(View.VISIBLE);
	    audioControls.setVisibility(View.VISIBLE);
	}


	
	public void init(){
		
		setSlide(0);
	}
	

	public void setSlide(int index){
		
		current = index;
		
		slideNumber.setText("Slide: " + String.valueOf(index+1) + " / " + String.valueOf(length));
		
		try {
			slide = slides.getJSONObject(index);
			
			setImage(slide.getString("image"));
			
			setAudio(slide.getString("audio"));
			

		} catch (JSONException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	public void setImage(String path) throws IOException{
		
		InputStream input = file.getData(path);
		
		System.out.println("here!");
	 Bitmap imageItem = BitmapFactory.decodeStream(input);
	 slideImage.setImageBitmap(imageItem);
     input.close();
		 	
	}
	
	public void setAudio (String path) throws IOException{
		
		InputStream audioStream = file.getData(path);
		
        new createAudio().execute(audioStream);
		
	}
	
	
	public void nextSlide(){
		
		  try {
				if (audioPlayer.isPlaying()){
					audioPlayer.stop();
			//		audioPlayer.release();
				}
			  } catch (IllegalStateException e){
					e.printStackTrace();
			  }
		
		
		if(current < (length -1)){
			setSlide(current+1);
		} 
	}
	
	public void previousSlide(){
		
		  try {
				if (audioPlayer.isPlaying()){
					audioPlayer.stop();
			//		audioPlayer.release();
				}
			  } catch (IllegalStateException e){
					e.printStackTrace();
			  }
		
		
		if(current >0){
			setSlide(current-1);
		} 
		
	}
	
	
	
	private class createAudio extends AsyncTask<InputStream, Void, File> {
	    /** The system calls this to perform work in a worker thread and
	      * delivers it the parameters given to AsyncTask.execute() **/
	    protected File doInBackground(InputStream... streams) {
		
	   InputStream input = streams[0];

	    File audiofile = new File("tmp.mp3");
	    
	    try{
	    
			if (audiofile.exists()){
				audiofile.delete();
			}
			
		audiofile = File.createTempFile("tmp", "mp3");

		OutputStream out =  new BufferedOutputStream(new FileOutputStream(audiofile));
		
		byte[] bytes = IOUtils.toByteArray(input);
		
		input.close();
		out.write(bytes);
		out.flush();
		out.close();	
	    }
	    
	    catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
			
	    return audiofile;
	    	
	    }
	    
	    /** The system calls this to perform work in the UI thread and delivers
	      * the result from doInBackground()   **/
	    protected void onPostExecute(File audioFile) {
	       
			Uri uri = Uri.fromFile(audioFile);
			audioPlayer = new MediaPlayer();

	        try {
				audioPlayer.setDataSource(thisContext, uri);
			

		        audioPlayer.prepare();
		        audioPlayer.start();
		        
		        
				SeekBar x = (SeekBar) audioView.findViewById(R.id.audio_progress);		
				ImageView audioIcon = (ImageView) audioView.findViewById(R.id.audio_icon);
				ProgressBar loading = (ProgressBar) audioView.findViewById(R.id.loading_icon);
				LinearLayout audioControls = (LinearLayout)audioView.findViewById(R.id.audio_controls);
				
			    x.setVisibility(View.VISIBLE);
			    audioIcon.setVisibility(View.VISIBLE);
			    loading.setVisibility(View.GONE);
			    audioControls.setVisibility(View.VISIBLE);
		        
		        System.out.println("ready");
		        audioProgress();

     
		        
				audioPlayer.setOnCompletionListener(new OnCompletionListener(){
					
				       public void onCompletion(MediaPlayer mp) {
			                nextSlide();
			            }
					
				});
		        
		        

			} catch (IllegalArgumentException | SecurityException
					| IllegalStateException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	    	
	    }


	}
	
	@Override
	public void onNext(){
		
		  try {
				if (audioPlayer.isPlaying()){
					audioPlayer.stop();
			//		audioPlayer.release();
				}
			  } catch (IllegalStateException e){
					e.printStackTrace();
			  }
		
	}
	
	
	public void audioProgress(){

   
		audioMonitor =  new Thread(new Runnable() { 
        	
        	public void run() throws IllegalStateException {
        		
       
        		while(audioPlayer.isPlaying()){
        			
        			
        			
        			if(audioPlayer.isPlaying()){
        			
        			int progress = audioPlayer.getCurrentPosition();
        			int duration = audioPlayer.getDuration();
        			
        			final int dl_progress = (int) (((double)progress*100.00)/ ((double)duration));
        			
        			if(thisContext instanceof Activity){
        		    Activity activity = (Activity)thisContext;
        		
        		    activity.runOnUiThread(new Runnable() {

	                        @Override
	                        public void run() {

	                          SeekBar x = (SeekBar) audioView.findViewById(R.id.audio_progress);
                           x.setProgress(dl_progress);
	                        }
	                    });
        			}
        			
        			}
        		}
           
        		
        	}
        });
		
		try {
		audioMonitor.start();
		} catch(IllegalStateException e){
			
		}
      
		
		
	}

}


/*			
SeekBar.OnSeekBarChangeListener l = new SeekBar.OnSeekBarChangeListener() {
	
	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
	
         if(fromUser){ 
        	int toSeekto = 	(int) ((audioPlayer.getDuration()*progress)/100);
        	audioPlayer.seekTo(toSeekto);         
         }			
	}
};
x.setOnSeekBarChangeListener(l);
*/		
