package org.dotlearn.app.modules;

import java.io.IOException;
import java.io.InputStream;


import org.dotlearn.app.R;
import org.dotlearn.app.models.CourseFile;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.ImageView;

public class ImageModule extends DataModule{

	public ImageModule(Context context, ViewGroup root, CourseFile file) {
		super(context, root, file);
		// TODO Auto-generated constructor stub

		ScrollView container = (ScrollView ) inflater.inflate(R.layout.item_image, root, false);
		
		

		this.setView(container);
	}
	
	@Override
	public void onLoad(String json){
		
         View layout = this.view;
 		try {
 			
 	     	JSONObject data = new JSONObject(json);
			((TextView)layout.findViewById(R.id.prompt)).setText(data.optString("caption"));
			((TextView)layout.findViewById(R.id.caption)).setText(data.optString("subtitle"));
			((TextView)layout.findViewById(R.id.description)).setText(data.optString("description"));

	
				
		
				 String itemPath = data.getString("location");
				 InputStream input = file.getData(itemPath);
				 Bitmap imageItem = BitmapFactory.decodeStream(input);
				 
				 ((ImageView)layout.findViewById(R.id.image)).setImageBitmap(imageItem);
				 
				 input.close();

				
			} catch (JSONException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


	
	}
	
	

}
