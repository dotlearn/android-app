package org.dotlearn.app.modules;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


import org.apache.commons.io.IOUtils;
import org.dotlearn.app.R;
import org.dotlearn.app.models.CourseFile;
import org.json.JSONException;
import org.json.JSONObject;

import com.joanzapata.pdfview.PDFView;
import com.joanzapata.pdfview.listener.OnPageChangeListener;
import android.content.Context;


import android.os.AsyncTask;

import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;



public class PDFModule extends DataModule implements OnPageChangeListener {


	PDFView pdfView;
	RelativeLayout pdfContainer;
	TextView pageCounter;
	int pageCount;
	OnPageChangeListener listener;
	TextView caption;
	

	
	public PDFModule(Context context, ViewGroup root, CourseFile file) {
		super(context, root, file);
		
		 listener = (OnPageChangeListener)this;
		
		
		pdfContainer= (RelativeLayout) inflater.inflate(R.layout.item_pdf, root, false);
		
		
		pdfView = (PDFView) pdfContainer.findViewById(R.id.pdfview);
		
		pageCounter = (TextView) pdfContainer.findViewById(R.id.pageNumber);
		
		caption = (TextView) pdfContainer.findViewById(R.id.caption);
		
		this.setView(pdfContainer);
	}
	
	
	@Override
	public void onLoad(String json){
	
		
		JSONObject data;
		try {
			data = new JSONObject(json);
			
			InputStream videoStream = file.getData(data.getString("location"));
			caption.setText(data.optString("caption"));
			 
			new createPDF().execute(videoStream);
	        
		} catch (JSONException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	
	}
	
	
	private class createPDF extends AsyncTask<InputStream, Void, File> {
	    /** The system calls this to perform work in a worker thread and
	      * delivers it the parameters given to AsyncTask.execute()  */
	    protected File doInBackground(InputStream... streams) {
		

	    File pdfFile = new File("tmp.pdf");
	    
	    try{
	    
			if (pdfFile.exists()){
				pdfFile.delete();
			}
			
		pdfFile = File.createTempFile("tmp", ".pdf");
		InputStream input = streams[0];
		OutputStream out =  new BufferedOutputStream(new FileOutputStream(pdfFile));
		
		byte[] bytes = IOUtils.toByteArray(input);
		
		input.close();
		out.write(bytes);
		out.flush();
		out.close();	
	    }
	    
	    catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
			
	    return pdfFile;
	    	
	    }
	    
	    /** The system calls this to perform work in the UI thread and delivers
	      * the result from doInBackground() */
	    protected void onPostExecute(File pdfFile) {
	    	
	    	System.out.println(pdfFile.toString());
	       
	    	pdfView.fromFile(pdfFile)

	       .showMinimap(true)
	        .enableSwipe(true)
	        	.onPageChange(listener)

	        .load();
	    	
	    	pageCount = pdfView.getPageCount();
	
	    	
	   
	    	
	    }


	}


	@Override
	public void onPageChanged(int page, int pageCount) {
		// TODO Auto-generated method stub

		if(page == 2){
			caption.setVisibility(View.GONE);
		}
	
		
		pageCounter.setText(String.valueOf(page) + " / " + String.valueOf(pageCount));
		
	}
	
	

}
