package org.dotlearn.app;


import java.util.Iterator;


import org.dotlearn.app.util.ImageDB;
import org.dotlearn.app.util.TreeDB;
import org.dotlearn.app.util.WebImageView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;

import android.app.Activity;


import android.content.Intent;

import android.os.Bundle;
import android.view.View;

import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import android.widget.EditText;

import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import org.dotlearn.app.models.*;


public class WebStoreActivity extends ActionBarActivity {

	public JSONArray lessons;
	public Activity activity;
	public JSONTree tree;
	public String serverData;
	public  String selected;
	public ImageDB imdb;

	@Override
	protected void onCreate(Bundle savedInstanceState) {



		System.out.println("Got to the webstore activity");

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_library);


		selected = getIntent().getExtras().getString("selected");
		Global global =((Global) this.getApplication());

		imdb = global.getImDB();



		if(global.getTree() != null){

			System.out.println("Using global tree");
			tree = global.getTree();



		} else{


			System.out.println("Using file tree");

			TreeDB treeDB = new TreeDB(this);

			JSONArray treeArray =  treeDB.getTree();

			try {
				tree = new JSONTree(treeArray);

			} catch (JSONException e) {


				e.printStackTrace();

				onError("Load error: ", "An error ocurred while loading the screen: " + e.getMessage());


			}

		}

		setNode(selected);


	}



	public  void onError(String title, String message){

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);


		final Context mcontext  = this;

		// set title
		alertDialogBuilder.setTitle(title);

		// set dialog message
		alertDialogBuilder.setMessage(message);

		alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				//do things

				Intent intent = new Intent(mcontext, org.dotlearn.app.MyLibraryActivity.class);
				intent.putExtra("error", true);
				startActivity(intent);

			}
		});


		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();

	}




	public void setNode(String id){

		tree.setCurrent(id);
		refreshView();


	}

	public  void refreshView(){

		updateHeader();

		try {
			updateChildren();
		} catch (JSONException e){
			e.printStackTrace();
		}

	}


	public void updateHeader(){

		String name = "default";
		String description = "none";
		String color = "#119900";
		String im = "none";


		try{
			JSONObject node = tree.getCurrent();
			name = node.getString("name");
			description = node.optString("description", "none");
			color = node.optString("color", "#ffffff");
			im = node.optString("image", "none");


		} catch (JSONException e){


			System.out.println("Exception! ");
			e.printStackTrace();

		}




		if(color.compareTo("inherit")==0){     //Hide the icon if it's not available

			//Update Color
			RelativeLayout banner = (RelativeLayout) findViewById(R.id.topBanner);

			String colorString = tree.getInherited(selected, "color");
			banner.setBackgroundColor(Color.parseColor(colorString));


		} else{

			//Update Color

			RelativeLayout banner = (RelativeLayout) findViewById(R.id.topBanner);
			banner.setBackgroundColor(Color.parseColor(color));

		}


		// Update Icon
		WebImageView image = (WebImageView) findViewById(R.id.nodeIcon);



		if(im.compareTo("none")==0){     //Hide the icon if it's not available
			image.setVisibility(View.INVISIBLE);
		} else{

			String imgURL = getString(R.string.server_images).concat(im);
			image.setImageUrl(imgURL);

		}





		// Update Name
		TextView title = (TextView)findViewById(R.id.nodeName);
		title.setText(name);

		//Update Description
		TextView desc = (TextView) findViewById(R.id.nodeDescription);




		float d = getResources().getDisplayMetrics().density;

		if(!(description.compareTo("none")==0)){//If description is provided

			RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) image.getLayoutParams();
			RelativeLayout.LayoutParams rlpt = (RelativeLayout.LayoutParams) title.getLayoutParams();
			rlp.addRule(RelativeLayout.CENTER_VERTICAL, 0);

			int left = (int)(d*30);
			int top = left;
			int right = (int)(d*10);

			rlp.setMargins(left, top, right, 0);
			rlpt.setMargins(0, 0, 0, 0);
			desc.setVisibility(View.VISIBLE);
			desc.setText(description);



		} else{                               //Otherwise
			RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) image.getLayoutParams();
			RelativeLayout.LayoutParams rlpt = (RelativeLayout.LayoutParams) title.getLayoutParams();
			rlp.addRule(RelativeLayout.CENTER_VERTICAL, 1);

			int left = (int)(d*40);
			int right = (int)(d*10);
			int top = (int)(d*5);

			rlp.setMargins(left, 0,right, 0);
			rlpt.setMargins(0, top, 0, 0);
			image.setLayoutParams(rlp);
			desc.setVisibility(View.GONE);

		}




		// Update Description
	}

	public void updateChildren()  throws JSONException{{


		final JSONArray categories = tree.getChildren();

		ListView listview = (ListView) findViewById(R.id.gridview);


		System.out.println(categories.toString());
		CategoryGridAdapter adapter = new CategoryGridAdapter(this, categories, tree, imdb);

		listview.setAdapter(adapter);
		listview.setClickable(true);

		listview.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				try {
					JSONObject selectedCategory = categories.getJSONObject(position);

					if(selectedCategory.getString("type").compareTo("lesson")==0){    //Target is a lesson

						searchLesson(selectedCategory.getString("id"));


					} else{                                                         // Target is another node

						searchNode(selectedCategory.getString("id"));

				}


				} catch (JSONException e) {
					e.printStackTrace();
				}

			}
		});



	}
	}





	public  void searchLesson(String id) throws  JSONException{



		Intent intent = new Intent(this, org.dotlearn.app.WebStoreLesson.class);

		JSONObject lesson =  tree.getById(id);

		intent.putExtra("lesson",lesson.toString());        //Initialize at the root
		startActivity(intent);
	}


	public  void searchNode(String id){


		Intent intent = new Intent(this, org.dotlearn.app.WebStoreActivity.class);
		intent.putExtra("selected",id);        //Initialize at the root
		finish();
		startActivity(intent);

	}
	
	


	@Override
	public void onBackPressed() {


		System.out.println("On back pressed");


		try {

			JSONObject current = tree.getCurrent();

			if(current.getString("id").compareTo("0")==0){           //If root, and press back, just go back home
                Intent intent = new Intent(this, MyLibraryActivity.class );
				intent.putExtra("error", false );
				startActivity(intent);
            } else{                // Otherwise, send it back to the parent node

				String parent = current.getString("parent");
				Intent intent = new Intent(this, org.dotlearn.app.WebStoreActivity.class);
				intent.putExtra("selected",parent);
				finish();
				startActivity(intent);

            }
		} catch (JSONException e) {
			e.printStackTrace();

			Intent intent = new Intent(this, MyLibraryActivity.class );
			intent.putExtra("error", true);
			startActivity(intent);
		}


	}
	
	


}



/*

		activity = this;



		String jsonString = getIntent().getExtras().getString("serverData");

		serverData = jsonString;


		System.out.println("In webstore activity, building tree");

		try {

			tree = new JSONTree(jsonString);

			System.out.println("Built tree, setting update");








		} catch (JSONException e) {


		}




*/