package org.dotlearn.app;

import java.io.File;

import org.dotlearn.app.models.ItemAdapter;
import org.dotlearn.app.util.ImageDB;
import org.dotlearn.app.util.LessonDB;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;

import org.dotlearn.app.util.AsyncResponse;
import org.dotlearn.app.util.JSONdb;
import org.dotlearn.app.util.POSTFetchJSON;
import org.dotlearn.app.util.WebImageView;

import android.support.v7.app.ActionBarActivity;

import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.DownloadManager.Request;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;


public class WebStoreLesson extends ActionBarActivity {
	
	POSTFetchJSON fetchJSON;
	
	JSONObject lesson;
	String json;
	
	public Boolean isDownloaded;
	public String downloadUrl;
	public String filename;
	public String downloadDirectory = "dotLearn";
	
	

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_library_preview);
		
		 json = getIntent().getExtras().getString("lesson");

	
		
		 isDownloaded = false;
		try {
			lesson = new JSONObject(json);
			preview(lesson);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	
	public void preview(JSONObject lesson){
		
		
	     ImageButton  downloadButton = (ImageButton)  findViewById(R.id.DownloadButton);
	     ImageButton playButton = (ImageButton) findViewById(R.id.InstalledPlayButton);
	     ImageButton deleteButton = (ImageButton) findViewById(R.id.DeleteButton);
		 TextView authorText = (TextView) findViewById(R.id.LibPreviewSubtitle);
	     TextView fileSizeView = (TextView) findViewById(R.id.LibFileSize);
		 WebImageView authorImg = (WebImageView) findViewById(R.id.AuthorImg);
		
		String description = lesson.optString("description", "Lorem Ipsum");
		String title = lesson.optString("name", "name");

		Global global =((Global) this.getApplication());

		ImageDB imdb = global.getImDB();
		LessonDB lessonDB = global.getLessonDB();



		try {
			JSONObject author = lesson.getJSONObject("author");

			authorText.setText(author.optString("name"));
			String authorImgUrl = getString(R.string.server_images).concat(author.getString("icon"));
			authorImg.setImageUrl(authorImgUrl);
		} catch (JSONException e) {
			e.printStackTrace();
		}



		String imageLoc = lesson.optString("image", "none");


		if (!(imageLoc.compareTo("none")==0)) {
			String imageURL = getString(R.string.server_images).concat(imageLoc);

			System.out.println("Trying to load image for Webstore lesson:");

			WebImageView image = (WebImageView) findViewById(R.id.LibPreviewImg);
			image.setImageUrl(imageURL, imdb);
			image.setScaleType(ImageView.ScaleType.CENTER_CROP);
		//	image.setPlaceholderImage(R.drawable.gray_logo);

		}
	
		
		filename = title.replaceAll(" ", "_").concat(".lsn");

		 ((TextView) findViewById(R.id.LibPreviewTitle)).setText(title);
		
		 ((TextView) findViewById(R.id.LibPreviewDescription)).setText(description);

		 

		
	        
	        int fileSize = lesson.optInt("size");
		    int  mb =(1024*1024);
		    
		    float fileSizeMB  = (float) (Math.round((((float)fileSize)/((float)(mb))*100))/100.0);
		
		    String fileSizeString = "File size: " + String.valueOf(fileSizeMB) + " MB";


	        
		    fileSizeView.setText(fileSizeString);
	       
		 downloadUrl = getString(R.string.server_files).concat(lesson.optString("id")).concat(".lsn");
	


	     
	     
	     
	     ListView itemsView = (ListView) findViewById(R.id.LessonItems);
	     
	     ItemAdapter adapter = new ItemAdapter( lesson.optJSONArray("items"), this);
	     itemsView.setAdapter(adapter);
	     
	     

	     	     

	     
		try {
			

			JSONObject lessons = lessonDB.getLessonIndex();

			 String lessonID = lesson.optString("id");
		    
			 
			 if(lessons.has(lessonID)){
		
		    	 JSONObject lessonInDB=  lessons.getJSONObject(lessonID);
		    	 
		    	 if(lessonInDB.optBoolean("downloaded")){
		        	 playButton.setVisibility(View.VISIBLE);
		        	 deleteButton.setVisibility(View.VISIBLE);
		    	 } else{
				       fileSizeView.setVisibility(View.VISIBLE);	 
				  	   downloadButton.setVisibility(View.VISIBLE);	  
		    	 }
		    	     	 
		     } else{
		    	
		       fileSizeView.setVisibility(View.VISIBLE);	 
		  	   downloadButton.setVisibility(View.VISIBLE);	 
		  	   
		  	   
		     }
		     
		} catch (IOException | JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	
	public void goPlay(View view){
		
		
		   JSONdb dbManager = new JSONdb(this);
		   
			String lessonLocation = lesson.optString("id") + ".lsn";
		
			try {
				((Global)this.getApplication()).setCourseFile(lessonLocation);
				

				   	 Intent intent = new Intent(this, ItemPlayerActivity.class);	      	    
				     startActivity(intent);
		
				
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	}
	
	
	public void deleteFile(View view){
		
		
		String id = lesson.optString("id");
		
	   JSONdb dbManager = new JSONdb(this);
	   
	   try {
		JSONObject db = dbManager.getDBFromFile();
		
		db.remove(id);
		
		dbManager.set(db);
		
		dbManager.save();
		
		String lessonLocation = dbManager.storageFolder + id + ".lsn";
		
		File lessonFile = new File(lessonLocation);
		
		if(lessonFile.exists()){
			lessonFile.delete();
		}
		
		
		Intent intent = new Intent(this, org.dotlearn.app.MyLibraryActivity.class);
	       startActivity(intent);
		
		
		
		
	} catch (IOException | JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	   
	   
		
		
	}
	
	
	  public void checkCourseFile(String filepath){
	    	
	    	ZipFile zip;
	    	
			try {
				zip = new ZipFile(filepath);
			 	ZipEntry coursefile = zip.getEntry("lesson.json");
			 	
			 	if (coursefile != null){
			 		System.out.println(filepath.concat(" has a course file"));
			 		
			 		InputStream input = zip.getInputStream(coursefile);
			 		
			 		StringWriter writer = new StringWriter();
			 		IOUtils.copy(input, writer, "UTF-8");
			 		String jsonstring = writer.toString();
			 		
			 		JSONObject jsoncourse = new JSONObject(jsonstring); 
			 
			 		
			 		System.out.println(jsoncourse.opt("description"));
			 		previewFile(filepath, jsoncourse);
			 		
			 		input.close();
			 		zip.close();
			 		
			 		
			 		// 
			 	} else{
			 		System.out.println(filepath.concat(" has no course file"));
			 		// No course file - send an android alert
			// 		onError(getString(R.string.notcoursefileerror));	
			 		zip.close();
			 	}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();                 // Problem reading the file
			//	onError(getString(R.string.filereaderror).concat(": ").concat(e.getMessage().toString()));	
			} catch (JSONException e) {
				// TODO Auto-generated catch block 
				e.printStackTrace();             // JSON formatting issues
			//	onError(getString(R.string.jsonerror));	
			}	
	    	
	    }
		
	   
	   public void previewFile(String zippath, JSONObject json){

	    	 try {	    		 
	    	 	 Intent intent = new Intent(this, org.dotlearn.app.FilePreviewActivity.class);
				((Global)this.getApplication()).setCourseFile(filename);
			   	 ((Global)this.getApplication()).setZip(zippath);
				 ((Global)this.getApplication()).setLesson(json);
		         startActivity(intent);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				Intent intent = new Intent(this, org.dotlearn.app.MyLibraryActivity.class);
			       startActivity(intent);
				e.printStackTrace();
			}
	    
	    }
	
	public void goDownload(View view){
		
		final JSONdb dbManager = new JSONdb(this);


		
	    try {

			final LessonDB db = new LessonDB(this);

			lesson.put("downloaded", false);

			db.putLesson(lesson);

			} catch (JSONException | IOException e) {
			// TODO Auto-generated catch block
			   e.printStackTrace();
		    }



	    	Intent intent = new Intent(getBaseContext(), org.dotlearn.app.MyLibraryActivity.class);
	    	intent.putExtra("download", lesson.optString("id"));
	    	startActivityForResult(intent , 1);
			
		
	
	}


	@Override
	public void onBackPressed() {

		Intent intent = new Intent(this, org.dotlearn.app.WebStoreActivity.class);
		intent.putExtra("selected",lesson.optString("parent", "0"));
		startActivity(intent);


	}

	
	
	/*	
	
	
	
	/*
	if(!isDownloaded){
		
	
		
		/*
		final File sdCard = Environment.getExternalStorageDirectory();
		final String folder = sdCard.getAbsolutePath() + "/dotLearn" ;
		final File dir = new File(folder );
		
		if (!dir.exists()) {
			if (dir.mkdirs()) {

			    }
	    }
		
        
		
	   Uri destination =	Uri.parse(Uri.fromFile(dir).toString().concat("/").concat(filename));
	  	
	   

	    
	   

	    isDownloaded = true;
	    

	    
	    view.setVisibility(View.GONE);		    
	    mProgressBar.setVisibility(View.VISIBLE);
	    

        new Thread(new Runnable() {

            @Override
            public void run() {

                boolean downloading = true;

                while (downloading) {

                    DownloadManager.Query q = new DownloadManager.Query();
                    q.setFilterById(downloadId);

                    Cursor cursor = dm.query(q);
                    cursor.moveToFirst();
                    int bytes_downloaded = cursor.getInt(cursor
                            .getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
                    int bytes_total = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));

                    if (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_SUCCESSFUL) {
                        downloading = false;
                    }

                    final int dl_progress = (int) ((bytes_downloaded * 100l) / bytes_total);
                    
                    
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {

                            mProgressBar.setProgress((int) dl_progress);

                        }
                    });


              
                    cursor.close();
                }
                
                
  
                
            	String imageURI;
        		try {
        			imageURI = getString(R.string.storage_address).concat(getString(R.string.client_id)).concat("/").concat(getString(R.string.server_images)).concat(lesson.getString("image"));
        		
        		} catch (JSONException e1) {
        			imageURI = getString(R.string.server_default_image);
        			e1.printStackTrace();
        		}
                
                
                String[] split = imageURI.split("/");
                
                final Context context = getBaseContext();;
                
                final String imageFileName = split[split.length-1];
                
                
                new Thread(new Runnable() { 
                	
                	public void run(){
                		
                		System.out.println(imageFileName);
    	                LessonDB db = new LessonDB(context);            
    	                db.putLesson(lesson, filename, imageFileName);
                		
                		

                		
                	}
                }).start();

                

                
               String imageFolder = folder + "/images";
               File imDir = new File(imageFolder);
               
	   			if (!imDir.exists()) {
					if (imDir.mkdirs()) {

					    }
			    }
                
     		   Uri destination =	Uri.parse(Uri.fromFile(imDir).toString().concat("/").concat(imageFileName));
     		   
     		   
     		   Uri source = Uri.parse(imageURI);
     		   
     		  Request request = new Request(source);
     		  request.setDestinationUri(destination);
     		  

     		  
     		 final long reqID =  dm.enqueue(request);
     		  
     		  
     		  
	     		 BroadcastReceiver onComplete=new BroadcastReceiver() {
		     		    public void onReceive(Context ctxt, Intent intent) {
		     		    long reference = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
		     		    	
		     		    
		     		    if(reqID == reference){
		     		    	
		     		    	
		     		       runOnUiThread(new Runnable() {

		                        @Override
		                        public void run() {
		                        	
		                        	

		                        	JSONObject data= new JSONObject();
		                        	try {
										data.put("lesson_id", lesson.opt("id"));
									  	data.put("client_id", getString(R.string.client_id));    
									} catch (JSONException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
		                                                //Here we reference stuff
		                    	    fetchJSON.setData(data);
		                    	    
		                    	    String address = getString(R.string.server_address).concat("recordDownload");
		                			
		                			String addresses[] = {address};
		                			
		                			fetchJSON.execute(addresses);
		                        }
		                    });
		     		    	
		     		      }
		     		   
		     		    }


		     		};
     		  
     		  
     		 registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
     		 
                

            }
        }).start();
	    
	    
	    
	    

	    

		
	}
	*/
	
}
