package org.dotlearn.app;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import org.dotlearn.app.models.JSONTree;
import org.dotlearn.app.models.LessonLibraryAdapter;

public class WebStoreCategory extends Activity {

	public JSONArray lessons;
	public JSONArray selected;
	public Context mContext;
	public JSONTree tree;
	public String categoryID;
	public JSONObject category;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_web_store_query);
		
		
		mContext = this;
		
		
		String jsonString = getIntent().getExtras().getString("serverData");
		categoryID = getIntent().getExtras().getString("categoryID");
		
		
		try {
			tree = new JSONTree(jsonString);
			tree.setCurrent(categoryID);
			
			category = tree.getCurrent();
			
			lessons = tree.getChildren();	
			viewSelected(lessons);
			
				
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		   	 Intent intent = new Intent(this, org.dotlearn.app.MyLibraryActivity.class);
		     startActivity(intent);
		}
		
		/*
		String searchBy = getIntent().getExtras().getString("searchBy");
        String searchTerm = getIntent().getExtras().getString("searchTerm");
        TextView header = (TextView)findViewById(R.id.Main);

		
		
			try {
				lessons = new JSONArray(jsonString);	
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			
			switch(searchBy){
			
			case "category":
				selected = selectByCategory(lessons, searchTerm);
			    header.setText(getString(R.string.category).concat(": ").concat(searchTerm));
				break;
				
			case "keyword":
				selected = selectByKeyWord(lessons, searchTerm);
				header.setText(getString(R.string.keyword).concat(": ").concat(searchTerm));
				break;
			
			
			}
			

			
			
			viewSelected(selected);
		
		*/
		
	}
	
	/*
	public JSONArray selectByCategory(JSONArray lessons, String category ){
		
		JSONArray selectedLessons = new JSONArray();
		
		for(int i = 0; i < lessons.length(); i++){
			JSONObject lesson = (JSONObject) lessons.opt(i);
			
		    if(lesson.optString("category").equals(category)){
		    	selectedLessons.put(lesson);
		    }
		}
		
		
		return selectedLessons;
		
	}
	
	public JSONArray selectByKeyWord(JSONArray lessons, String keyword ){
		
		JSONArray selectedLessons = new JSONArray();
		
		for(int i = 0; i < lessons.length(); i++){
			
			JSONObject lesson = (JSONObject) lessons.opt(i);
			
		    if(matchByKeyWord(lesson, keyword) || matchByTitle(lesson, keyword)||matchByDesc(lesson, keyword)){
		    	selectedLessons.put(lesson);
		    }
		}
			
		return selectedLessons;
		
	}
	
	
	public Boolean matchByKeyWord(JSONObject lesson, String toMatch){
		
		Boolean matched = false;
		
		try {
			JSONArray keywords = lesson.getJSONArray("keywords");
			
			for(int j=0; j < keywords.length(); j++){
				
				
				 String keyword = keywords.getString(j);
				 if(keyword.contains(toMatch)|| keyword.equalsIgnoreCase(toMatch)){
					 matched = true;
				 }
			}
			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		return matched;
	}
	
	public Boolean matchByTitle(JSONObject lesson, String toMatch){
		
		String title = lesson.optString("name");
		return (title.contains(toMatch)|| title.equalsIgnoreCase(toMatch));
	}
	
	public Boolean matchByDesc(JSONObject lesson, String toMatch){
		
		String desc = lesson.optString("name");
		
		return (desc.contains(toMatch));
	}
	*/
	
	public void viewSelected(final JSONArray selected){
		
		TextView categoryView = (TextView)findViewById(R.id.category);
		categoryView.setText(category.optString("name"));
		
		
		ListView listview = (ListView)findViewById(R.id.gridview);
		
		LessonLibraryAdapter adapter = new LessonLibraryAdapter(mContext, selected);
		
		listview.setAdapter(adapter);
		
		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				
				System.out.println("here");
				
				try {
					
					JSONObject lesson = selected.getJSONObject(position);
					Intent intent = new Intent(mContext, org.dotlearn.app.WebStoreLesson.class);
					
					intent.putExtra("lesson", lesson.toString());     	    
				    startActivity(intent);
				
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}

	    });
		
		
		
	}
	
	
	
	
}
