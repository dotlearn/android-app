package org.dotlearn.app;

import org.dotlearn.app.models.CourseFile;
import org.dotlearn.app.models.UserData;
import org.dotlearn.app.modules.HTMLModule;
import org.dotlearn.app.modules.ModuleManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.VideoView;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 * 
 * @see SystemUiHider
 */
@SuppressLint("NewApi")
public class ItemPlayerActivity extends ActionBarActivity {


    private GestureDetector gestureDetector;
	public JSONObject lesson;
	public int selectedLesson;
	public JSONArray items;
	public int currentItem;
	public LinearLayout itembox;
	public VideoView videoView;

	public MediaController mc;
	public Bitmap imageItem;
	public String lastType = "none";
	private OnTouchListener l;
	public Global global;
	public CourseFile file;
	public LinearLayout lessonEnd;
	public ScrollView scrollview;

	public ModuleManager mm;
	
	public HTMLModule itemHTML;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_item_player);

	
		
		
		
		 global = ((Global)this.getApplication());
		 file= global.getCourseFile();
		

		
		  gestureDetector = new GestureDetector(this, new GestureListener());
		
		

		final View screen = findViewById(R.id.fullscreen_content);
		
		
		l = new OnTouchListener(){
			public boolean onTouch(View v, MotionEvent event) {
				 
                if (gestureDetector.onTouchEvent(event)) {
       
                    return true;
                }
                return false;
            }
		};
		
		
		
		
		screen.setOnTouchListener(l);
		
		onLoad();

		

	}
	
	
	public boolean fullScreenVideo(){
	
		int orientation = getResources().getConfiguration().orientation;
		
		boolean isfullscreen = false;
		
		if(orientation != 0){
			if (videoView != null){
				
				if (videoView.isPlaying()){
					isfullscreen = true;
					
					
				}
			}
			
		}
		
		return isfullscreen;
		
		
		
	}
	
	public void onConfigurationChanged(Configuration newConfig) {
	    super.onConfigurationChanged(newConfig);

	    // Checks the orientation of the screen
	    if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
	             getSupportActionBar().hide();
	             ((View) findViewById(R.id.lesson_meta)).setVisibility(View.GONE);
	    } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
	         getSupportActionBar().show();
	         ((View) findViewById(R.id.lesson_meta)).setVisibility(View.VISIBLE);
	    }
	}
	
	
	
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);

	}
	
	
	public void onLoad(){
		
		switch(file.type){
		case "lesson":
			lesson = file.json;
			global.setLesson(lesson);
			break;
		case "course":
			break;
		
		}

		try {
			items = lesson.getJSONArray("items");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		setUI();
		
		mm = new ModuleManager(this, itembox, global);
		setItem(1);
		
	}

	
	public void setUI(){
		
	    String lessonNum = String.valueOf(selectedLesson+1);
		String title = "Lesson ".concat(lessonNum).concat(": ");
		int lessonLength = items.length();
		
		try {
			title = title.concat(lesson.getString("name"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		setTitle(title);
		TextView progressTotal = (TextView) findViewById(R.id.progress_total);
		TextView progressCurrent= (TextView) findViewById(R.id.progress_current);
		
		progressTotal.setText(String.valueOf(lessonLength));
		progressCurrent.setText(String.valueOf(currentItem));
		
		itembox = (LinearLayout) findViewById(R.id.item_box);
		
		
		
		
	     LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	     
	     scrollview = (ScrollView) inflater.inflate(R.layout.screen_lesson_end, itembox, false);
	     
	     lessonEnd =(LinearLayout) scrollview.findViewById(R.id.panelContainer);
	     
	
	     itembox.addView(scrollview);
	     
	     scrollview.setVisibility(View.GONE);
			
	     
	}
	
	
	public void back2Browser(View button){
		mm.onNext(getCurrentModule());
	   	 Intent intent = new Intent(this, MyLibraryActivity.class);
	     startActivity(intent);
	}
	
	public void previousItem(View button){
		mm.onNext(getCurrentModule());
		previous();
	}
	
	
	public void previous(){
		
		scrollview.setVisibility(View.GONE);
		
		if (currentItem == 1){
		   	 Intent intent = new Intent(this, MyLibraryActivity.class);

		     startActivity(intent);
		} else {
			setItem(currentItem - 1);
		}
	}
	
	public void next(){
				
		if (currentItem == items.length()){
			lessonEnd();
			currentItem++;
		} else if(currentItem == (items.length()+1)) {
		  	 Intent intent = new Intent(this, MyLibraryActivity.class);

		    startActivity(intent);
		} else {
			setItem(currentItem + 1);
		}
	}
	

	
	public void nextItem(View button){
    		mm.onNext(getCurrentModule());
           next();
	}
	
	public void refreshItem(View button){
		setItem(currentItem);
	}
	
	public void setItem(int itemNum){
			
		currentItem = itemNum;
		TextView progressCurrent= (TextView) findViewById(R.id.progress_current);
		progressCurrent.setText(String.valueOf(currentItem).concat(" / ").concat(String.valueOf(items.length())));
		
		TextView itemLabel = (TextView) findViewById(R.id.item_label);
		
		
		UserData ud = new UserData(global, currentItem);
		
		
		try {
			JSONObject item = items.getJSONObject((itemNum-1));

			String data = item.getString("data");
			String module = item.getString("module");
			String label = item.optString("label");
			itemLabel.setText(label);	

			mm.onLoad(item, ud);
			
		

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public JSONObject getCurrentItem(){
		JSONObject item = new JSONObject();
		try {
			item = items.getJSONObject((currentItem-1));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return item;
	}
	
	public String getCurrentModule(){
		
	
		JSONObject item = getCurrentItem();
	    String module = item.optString("module");
		
		return module;
	}
	
	
	public class GestureListener extends SimpleOnGestureListener{
	       
        private static final int SWIPE_MIN_DISTANCE = 120;
        private static final int SWIPE_THRESHOLD_VELOCITY = 200;
       
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, 
                                        float velocityX, float velocityY) {
           
            if(e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && 
                         Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
            	
            	           System.out.println("next");
            	           next();
            	           
                //From Right to Left
                return true;
            }  else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE &&
                         Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
            	
            	System.out.println("back");
            	previous();
                //From Left to Right
                return true;
            }
           
            if(e1.getY() - e2.getY() > SWIPE_MIN_DISTANCE && 
                        Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY) {
                //From Bottom to Top
                return true;
            }  else if (e2.getY() - e1.getY() > SWIPE_MIN_DISTANCE && 
                        Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY) {
                //From Top to Bottom
                return true;
            }
            return false;
        }
        @Override
        public boolean onDown(MotionEvent e) {
            //always return true since all gestures always begin with onDown and<br>
            //if this returns false, the framework won't try to pick up onFling for example.
            return true;
        }
    }
	
	
	public void lessonEnd(){


	     scrollview.setVisibility(View.VISIBLE);
	    
	 
		TextView progressCurrent= (TextView) findViewById(R.id.progress_current);
		progressCurrent.setText("Review");
		
		
		
		
		lessonEnd.removeAllViews();
		lessonEnd.setOrientation(LinearLayout.VERTICAL);
		
		int lessonEndItems = 0;


		try {
			JSONArray items = lesson.getJSONArray("items");
			JSONObject item;
			for (int i=0; i<items.length(); i++){
				     item = items.getJSONObject(i);
	                 String data = item.getString("data");		
				     
				     String module = item.optString("module");

				 	
				     UserData ud = new UserData(global, i+1);
				     
				     View view =  mm.onLessonEnd(module, data, ud );
				     
				     
				     if(view != null){
				    	 lessonEnd.addView(view);
				    	 lessonEndItems++;
				     }
				     

				    
				     
			}
			
			if(lessonEndItems==0){
				back2Browser(lessonEnd);
			}
		} catch (JSONException e) {
			e.printStackTrace();// TODO Auto-generated catch block
			back2Browser(lessonEnd);
		}
		

	}

	
	
}



