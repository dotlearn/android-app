package org.dotlearn.app;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.view.View;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 */
public class SettingsActivity extends ActionBarActivity {

	
	private SharedPreferences.OnSharedPreferenceChangeListener listener;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setupActionBar();
		
		setContentView(R.layout.activity_settings);
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			// Show the Up button in the action bar.
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {           // Up Navigation to home screen
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		
	//	addPreferencesFromResource(R.xml.settings);
		 //settingsSetup();
		 
		 
		//setupSimplePreferencesScreen();
	}
	
	public void deleteFiles(View view){
		
		
		confirmDelete();
	}
	
	private void confirmDelete(){
		
		
		 AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

		          alertDialogBuilder.setTitle("Delete all lessons?");
	
		          alertDialogBuilder.setMessage("Do you want to delete all these lessons?");
		
		          alertDialogBuilder.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
		        	  
		        	                  public void onClick(DialogInterface dialog,int id) {
		        	  
		        	                 
		        	                	  	removeFiles();
		        	                  }
		        	  
		        	                });
		        	  
		        	           // set negative button: No message
		        	  
		        	           alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
		        	  
		        	                public void onClick(DialogInterface dialog,int id) {
		        	  
		        	                      // cancel the alert box and put a Toast to the user
		        	  
		        	                      dialog.cancel();
		        	  
		        	                  }
		        	 
		        	              });

		        	           
		        	           AlertDialog alertDialog = alertDialogBuilder.create();
		        	     
		        	                    // show alert
		        	      
		        	                    alertDialog.show();


	}
	
	public void removeFiles(){
		
		((Global)this.getApplication()).deleteFiles();
		
		Intent intent = new Intent(getBaseContext(), org.dotlearn.app.MyLibraryActivity.class);
        startActivity(intent);
	}

	public void settingsSetup(){
	   
		SharedPreferences prefs = 
			    PreferenceManager.getDefaultSharedPreferences(this);
		
		
		listener = new SharedPreferences.OnSharedPreferenceChangeListener() {
			  public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
			    // When user settings change
			  }
			};

	    prefs.registerOnSharedPreferenceChangeListener(listener);
			    
	    
	}
}
